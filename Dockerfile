ARG DEBIAN_FRONTEND=noninteractive

FROM debian:bookworm-slim as builder
ENV LC_ALL=C.UTF-8
RUN apt-get update \
 && apt-get install -y python3 \
    python3-pip python3-xdg ssl-cert

COPY . /tmp/proxy
RUN cd /tmp/proxy && python3 setup.py install


# Build server container
FROM debian:bookworm-slim as server
LABEL maintainer Andrej Shadura <andrew.shadura@collabora.co.uk>
ENV LC_ALL=C.UTF-8

RUN apt-get update \
 && apt-get install -y python3-minimal python3-xdg ssl-cert

COPY --from=builder /usr/local /usr/local

RUN mkdir -p /etc/obs-proxy
COPY proxy.conf /etc/obs-proxy/

#COPY docker-entrypoint.sh /opt/docker-entrypoint.sh
#ENTRYPOINT ["/opt/docker-entrypoint.sh"]
