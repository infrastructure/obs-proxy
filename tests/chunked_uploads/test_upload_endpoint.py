from http import HTTPStatus
from typing import Optional

import pytest
from quart import Quart
from werkzeug.datastructures import ContentRange

from obs_proxy.chunked_uploads.utils import hash_file


@pytest.mark.asyncio
async def test_smoke(test_app: Quart, md5: str):
    client = test_app.test_client()

    response = await client.patch(f'/-/jobs/{md5}')

    assert response.status_code == HTTPStatus.BAD_REQUEST


@pytest.mark.asyncio
async def test_invalid_jobid(test_app: Quart):
    client = test_app.test_client()

    response = await client.patch('/-/jobs/foobad')

    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert b"incorrect jobid" in await response.data


@pytest.mark.asyncio
async def test_missing_content_length(test_app: Quart):
    client = test_app.test_client()

    response = await client.patch(
        '/-/jobs/d41d8cd98f00b204e9800998ecf8427e',
        headers={
            "Patch-Content-Range": "bytes 1-2/3",
        },
    )

    assert response.status_code == HTTPStatus.LENGTH_REQUIRED


@pytest.mark.parametrize(
    ("content_range", "status_code", "error_message"),
    [
        (
            ContentRange(start=123, stop=234, length=999, units='bytes').to_header(),
            HTTPStatus.BAD_REQUEST,
            "length must match",
        ),
        (
            ContentRange(start=123, stop=234, length=999, units='furlongs').to_header(),
            HTTPStatus.BAD_REQUEST,
            "Unsupported unit",
        ),
        (
            "*/0",
            HTTPStatus.BAD_REQUEST,
            "A range is required",
        ),
    ],
)
@pytest.mark.asyncio
async def test_patch_range_parser(
        test_app: Quart,
        md5,
        content_range: ContentRange,
        status_code: int,
        error_message: Optional[str],
):
    client = test_app.test_client()
    worker_id = "worker:1"

    response = await client.patch(
        f'/{worker_id}/jobs/{md5}',
        headers={
            'Patch-Content-Range': content_range,
            'Content-Length': 0,
        },
        data=b"",
    )

    assert response.status_code == status_code
    if error_message:
        assert error_message.encode('UTF-8') in await response.data


@pytest.mark.asyncio
async def test_patch_sequential_upload(
        test_app: Quart,
        md5: str,
):
    client = test_app.test_client()
    worker_id = "worker:1"
    content_range = ContentRange(start=0, stop=512, length=2048, units='bytes')

    chunk_size = 512

    for step in range(4):
        content_range.start = step * chunk_size
        content_range.stop = content_range.start + chunk_size
        payload = (b'%d' % step) * chunk_size

        response = await client.patch(
            f'/{worker_id}/jobs/{md5}',
            headers={
                'Patch-Content-Range': content_range.to_header(),
                'Content-Length': len(payload),
            },
            data=payload,
        )

        assert response.status_code == HTTPStatus.OK, "replied with {await response.data}"

    test_file = test_app.app.spool_dir / worker_id / md5
    file_hash = await hash_file(test_file)
    assert file_hash == "1e67b0c6adaeb8e87b57b2e3ced03463"

    response = await client.head(f'/{worker_id}/jobs/{md5}')
    print(f"{response.headers = }")

    etag, weak = response.get_etag()
    assert etag == file_hash


@pytest.mark.asyncio
async def test_patch_incomplete_upload(
        test_app: Quart,
        md5: str,
):
    client = test_app.test_client()
    worker_id = "worker:1"
    content_range = ContentRange(start=0, stop=512, length=2048, units='bytes')

    chunk_size = 512
    short_chunk_size = 64

    for step in range(4):
        content_range.start = step * chunk_size
        content_range.stop = content_range.start + chunk_size
        payload = (
            (b'%d' % step) * chunk_size
            if step != 3
            else (b'%d' % step) * short_chunk_size
        )

        response = await client.patch(
            f'/{worker_id}/jobs/{md5}',
            headers={
                'Patch-Content-Range': content_range.to_header(),
                'Content-Length': chunk_size,
            },
            data=payload,
        )

        assert response.status_code == (
            HTTPStatus.OK if step != 3 else HTTPStatus.REQUEST_ENTITY_TOO_LARGE
        ), "replied with {await response.data}"
