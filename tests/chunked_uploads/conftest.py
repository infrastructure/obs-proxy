import asyncio
import hashlib
import logging
import random
from typing import Generator

import pytest
from pytest import LogCaptureFixture  # noqa: PT013
from quart import Quart


@pytest.fixture
def app(caplog: LogCaptureFixture, tmp_path) -> Generator[Quart, None, None]:
    from obs_proxy.chunked_uploads import chunked_uploads

    caplog.set_level(logging.DEBUG)
    app = Quart(__name__)
    app.spool_dir = tmp_path / "obs-proxy-spool"
    app.register_blueprint(chunked_uploads, url_prefix="/<path:worker_id>/jobs")
    return app


@pytest.fixture
def test_app(app: Quart) -> Generator[Quart, None, None]:
    return app.test_app()


@pytest.fixture
async def http_app(app: Quart, unused_tcp_port: int) -> Generator[tuple[Quart, int], None, None]:
    shutdown_event = asyncio.Event()
    task = asyncio.create_task(
        app.run_task(
            port=unused_tcp_port,
            shutdown_trigger=shutdown_event.wait,
        ),
    )
    try:
        yield app, unused_tcp_port
    finally:
        shutdown_event.set()
        await task
        task.cancel()


@pytest.fixture
def md5() -> str:
    return hashlib.md5(random.randbytes(32)).hexdigest()


@pytest.fixture
def binary(size=None) -> bytes:
    if not size:
        size = 4 * 1024 * 1024 + random.randint(0, 255)
    return random.randbytes(size)
