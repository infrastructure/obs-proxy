from pathlib import Path

import aiofiles
import httpx
import pytest
from quart import Quart

from obs_proxy.chunked_uploads.client import upload_chunked
from obs_proxy.chunked_uploads.utils import hash_file

CHUNK_SIZE = 512 * 1024


@pytest.mark.asyncio
async def test_client_uploading_to_server(
        http_app: tuple[Quart, int],
        tmp_path: Path,
        binary: bytes,
        md5: str,
):
    app, port = http_app

    tmp_file = tmp_path / "tempfile"
    tmp_file.write_bytes(binary)

    worker_id = "-"

    size = len(binary)

    async with aiofiles.open(tmp_file, 'rb') as file:
        async with httpx.AsyncClient() as client:
            print(f"uploading {size} bytes as job {md5}")
            await upload_chunked(
                client,
                f'http://127.0.0.1:{port}/{worker_id}/jobs/{md5}',
                data=file,
                size=size,
                chunk_size=CHUNK_SIZE,
            )

    test_file = app.spool_dir / worker_id / md5
    file_hash = await hash_file(test_file)

    expected_file_hash = await hash_file(tmp_file)

    assert file_hash == expected_file_hash, "File did not upload correctly"
