from pathlib import Path

import aiofiles
import httpx
import pytest
from pytest_httpx import HTTPXMock
from werkzeug.http import quote_etag

from obs_proxy.chunked_uploads.client import (
    ChunkedUploadError,
    ChunkedUploadVerificationError,
    upload_chunked,
)

SIZE = 4 * 1024 * 1024
CHUNK_SIZE = 512 * 1024


@pytest.mark.asyncio
async def test_upload_chunked_timeout(
        tmp_path: Path,
        httpx_mock: HTTPXMock,
):
    """Verify that protocol errors are handled properly.

    Check that timeouts and other protocol errors cause the failed chunk to be repeatedly
    re-uploaded, and, failing that, ultimately raise an appropriate exception allowing
    the issue to be reported.
    """
    httpx_mock.add_response(
        match_headers={
            "Content-Length": str(CHUNK_SIZE),
        },
    )

    break_at_chunk = 5
    break_at = "bytes %d-%d/%d" % (
        break_at_chunk * CHUNK_SIZE,
        (break_at_chunk + 1) * CHUNK_SIZE - 1,
        SIZE,
    )

    httpx_mock.add_exception(
        httpx.ReadTimeout("Simulated timeout"),
        match_headers={
            "Patch-Content-Range": break_at,
        },
    )

    tmp_file = tmp_path / "tempfile"
    with tmp_file.open("wb") as f:
        f.truncate(SIZE)

    async with httpx.AsyncClient() as client:
        file = await aiofiles.open(tmp_file, 'rb')
        with pytest.raises(
                ChunkedUploadError,
                match=f"Upload error at range {break_at} .attempt 4.: Simulated timeout",
        ) as excinfo:
            await upload_chunked(
                client,
                "http://example.org/upload",
                data=file,
                size=SIZE,
                chunk_size=CHUNK_SIZE,
            )
        print(excinfo.value)


@pytest.mark.asyncio
async def test_upload_chunked_wrong_response(
        tmp_path: Path,
        httpx_mock: HTTPXMock,
):
    """Verify that unexpected HTTP responses interrupt the upload immediately and raise an exception"""
    httpx_mock.add_response(
        match_headers={
            "Content-Length": str(CHUNK_SIZE),
        },
    )

    break_at_chunk = 5
    break_at = "bytes %d-%d/%d" % (
        break_at_chunk * CHUNK_SIZE,
        (break_at_chunk + 1) * CHUNK_SIZE - 1,
        SIZE,
    )

    httpx_mock.add_response(
        status_code=418,
        match_headers={
            "Patch-Content-Range": break_at,
        },
    )

    tmp_file = tmp_path / "tempfile"
    with tmp_file.open("wb") as f:
        f.truncate(SIZE)

    async with httpx.AsyncClient() as client:
        file = await aiofiles.open(tmp_file, 'rb')
        with pytest.raises(
                ChunkedUploadError,
                match=f"Upload error at range {break_at} .attempt 0.: .*418 I'm a teapot.*",
        ) as excinfo:
            await upload_chunked(
                client,
                "http://example.org/upload",
                data=file,
                size=SIZE,
                chunk_size=CHUNK_SIZE,
            )

        print(excinfo.value)


@pytest.mark.asyncio
async def test_upload_chunked_transient_server_error(
        tmp_path: Path,
        httpx_mock: HTTPXMock,
):
    """Verify that unexpected HTTP responses in 5xx range cause a retry and raise an exception if persist"""
    httpx_mock.add_response(
        match_headers={
            "Content-Length": str(CHUNK_SIZE),
        },
    )

    break_at_chunk = 5
    break_at = "bytes %d-%d/%d" % (
        break_at_chunk * CHUNK_SIZE,
        (break_at_chunk + 1) * CHUNK_SIZE - 1,
        SIZE,
    )

    failed_once = False

    def fail_once(request: httpx.Request):
        if not failed_once:
            return httpx.Response(
                status_code=500,
            )
        else:
            return httpx.Response(
                status_code=200,
            )

    httpx_mock.add_callback(
        fail_once,
        match_headers={
            "Patch-Content-Range": break_at,
        },
    )

    tmp_file = tmp_path / "tempfile"
    with tmp_file.open("wb") as f:
        f.truncate(SIZE)

    async with httpx.AsyncClient() as client:
        file = await aiofiles.open(tmp_file, 'rb')
        with pytest.raises(
                ChunkedUploadError,
                match=f"Upload error at range {break_at} .attempt 4.: .*500 Internal Server Error.*",
        ) as excinfo:
            await upload_chunked(
                client,
                "http://example.org/upload",
                data=file,
                size=SIZE,
                chunk_size=CHUNK_SIZE,
            )

        print(excinfo.value)


@pytest.mark.asyncio
async def test_upload_chunked_complete(
        tmp_path: Path,
        httpx_mock: HTTPXMock,
):
    """Verify the complete upload is followed by a hashsum verification"""
    httpx_mock.add_response(
        match_headers={
            "Content-Length": str(CHUNK_SIZE),
        },
    )

    # this is the MD5 of 4 MB zeroes
    expected_md5 = "b5cfa9d6c8febd618f91ac2843d50a1c"

    httpx_mock.add_response(
        method="HEAD",
        headers={
            "ETag": quote_etag(expected_md5 + "foo"),
        },
    )

    tmp_file = tmp_path / "tempfile"
    with tmp_file.open("wb") as f:
        f.truncate(SIZE)

    async with httpx.AsyncClient() as client:
        file = await aiofiles.open(tmp_file, 'rb')
        with pytest.raises(
                ChunkedUploadVerificationError,
        ) as excinfo:
            await upload_chunked(
                client,
                "http://example.org/upload",
                data=file,
                size=SIZE,
                chunk_size=CHUNK_SIZE,
            )

        assert excinfo.value.expected == expected_md5
