import asyncio

import pytest
from jsonrpc.jsonrpc2 import JSONRPC20Request

from obs_proxy.rpcqueue import RPCQueue


@pytest.mark.asyncio
async def test_rpcqueue():
    import dataclasses

    @dataclasses.dataclass
    class FauxMsg:
        _id: int
        msg: str

    fauxmsg1 = FauxMsg(_id=1, msg="first reply")
    fauxmsg2 = FauxMsg(_id=2, msg="second reply")

    async def caller(q):
        async def t(x):
            print(f"{x = }")

        resp1 = await q.call(JSONRPC20Request(_id=1, method="ping"))
        print(f"{resp1 = }")
        resp2 = await q.call(JSONRPC20Request(_id=2, method="pong"))
        print(f"{resp2 = }")
        return resp1, resp2

    async def replier(q):
        await asyncio.sleep(1)
        print("slept")
        await q.reply(fauxmsg1)
        print("replied")
        await asyncio.sleep(1)
        await q.reply(fauxmsg2)
        print("replied again")

    q = RPCQueue()
    loop = asyncio.get_running_loop()
    task1 = loop.create_task(caller(q))
    print(f"{q = }")
    task2 = loop.create_task(replier(q))
    await asyncio.sleep(3)
    print(f"{q = }")
    done, _ = await asyncio.wait([task1], timeout=10)
    resp1, resp2 = task1.result()
    assert resp1 == fauxmsg1
    assert resp2 == fauxmsg2
    await asyncio.wait([task2], timeout=10)
