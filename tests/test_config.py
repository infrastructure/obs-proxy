import io

from obs_proxy.config import (
    AuthConfig,
    BackendConfig,
    ClientConfig,
    Config,
    ServerConfig,
)

basic_config = """
[server]
host = 1.2.3.4
port = 1234
http2 = yes
tls = no
[client]
host = 2.3.4.5
port = 4321
[backend]
srcserver = backend1:2345
repserver = backend2:5252
[auth]
token = test-token
[workers]
startport = 6123
endport = 6134
"""


basic_config_dict = {
    'server': {
        'host': '1.2.3.4',
        'port': 1234,
        'prefix': '',
        'buffer_uploads': False,
        'certfile': None,
        'http2': True,
        'insecure': False,
        'keyfile': None,
        'tls': False,
    },
    'client': {
        'host': '2.3.4.5',
        'port': 4321,
        'buffer_uploads': True,
    },
    'auth': {
        'username': None,
        'password': None,
        'token': 'test-token',
    },
    'backend': {
        'repserver_uri': 'http://backend2:5252',
        'srcserver_uri': 'http://backend1:2345',
    },
    'debug': False,
    'tracedir': None,
    'cachedir': None,
    'worker_ports': {
        'start': 6123,
        'end': 6134,
    },

}


def test_config_parser():
    f = io.StringIO(basic_config)
    c = Config.parse_file(f)
    assert c.server == ServerConfig(
        host='1.2.3.4',
        port=1234,
        prefix="",
        http2=True,
        tls=False,
        insecure=False,
        buffer_uploads=False,
        keyfile=None,
        certfile=None,
    )
    assert c.client == ClientConfig(
        host='2.3.4.5',
        port=4321,
        buffer_uploads=True,
    )
    assert c.auth == AuthConfig(
        username=None,
        password=None,
        token='test-token',
    )
    assert c.backend == BackendConfig(
        srcserver_uri='http://backend1:2345',
        repserver_uri='http://backend2:5252',
    )
    assert c.debug is False
    assert c.worker_ports == range(6123, 6135)
    assert c.tracedir is None
    assert c.cachedir is None


def test_config_parser_dict():
    f = io.StringIO(basic_config)
    c = Config.parse_file(f)
    assert c.as_dict() == basic_config_dict


mini_config = """
[server]
host = 3.4.5.6
port = 5678
prefix = /baz
[auth]
username = luser
password = drosswap
"""


def test_mini_config():
    f = io.StringIO(mini_config)
    c = Config.parse_file(f)
    assert c == Config(
        server=ServerConfig(
            host='3.4.5.6',
            port=5678,
            prefix='/baz',
            http2=False,
            tls=False,
            insecure=False,
            buffer_uploads=False,
            keyfile=None,
            certfile=None,
        ),
        client=ClientConfig(
            host='0.0.0.0',
            port=5000,
            buffer_uploads=True,
        ),
        auth=AuthConfig(
            username='luser',
            password='drosswap',
            token=None,
        ),
        backend=BackendConfig(
            srcserver_uri=None,
            repserver_uri=None,
        ),
        worker_ports=None,
        debug=False,
        tracedir=None,
        cachedir=None,
    )


def test_mini_config_update():
    f = io.StringIO(mini_config)
    mini_conf = Config.parse_file(f)

    f = io.StringIO(basic_config)
    full_conf = Config.parse_file(f)

    mini_conf.update_from_dict(full_conf.as_dict())

    assert mini_conf.as_dict() == basic_config_dict

    assert mini_conf.worker_ports == range(6123, 6135)
