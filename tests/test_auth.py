import logging
from http import HTTPStatus
from typing import Generator

import pytest
from pytest import LogCaptureFixture  # noqa: PT013
from quart import Quart

from obs_proxy.config import config
from obs_proxy.server import require_auth


@pytest.fixture
def app(caplog: LogCaptureFixture, tmp_path) -> Generator[Quart, None, None]:
    caplog.set_level(logging.DEBUG)
    app = Quart(__name__)

    @app.route('/ping')
    @require_auth
    async def ping():
        return "ok"

    return app


@pytest.fixture
def test_app(app: Quart) -> Generator[Quart, None, None]:
    return app.test_app()


@pytest.mark.asyncio
async def test_userpass(test_app: Quart):
    config.auth.username = "foobar"
    config.auth.password = "barfoo"
    client = test_app.test_client()

    response = await client.get(
        '/ping',
        scope_base={
            "server": ("127.0.0.1", config.server.port),
        },
    )

    assert response.status_code == HTTPStatus.UNAUTHORIZED

    response = await client.get(
        '/ping',
        auth=(config.auth.username, config.auth.password),
        scope_base={
            "server": ("127.0.0.1", config.server.port),
        },
    )

    assert response.status_code == HTTPStatus.OK


@pytest.mark.asyncio
async def test_token(test_app: Quart):
    config.auth.token = "barfoobarbaz"
    client = test_app.test_client()

    response = await client.get(
        '/ping',
        scope_base={
            "server": ("127.0.0.1", config.server.port),
        },
    )

    assert response.status_code == HTTPStatus.UNAUTHORIZED

    response = await client.get(
        '/ping',
        headers={
            "Authorization": f"Bearer {config.auth.token}",
        },
        scope_base={
            "server": ("127.0.0.1", config.server.port),
        },
    )

    assert response.status_code == HTTPStatus.OK
