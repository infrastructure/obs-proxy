import logging
from dataclasses import dataclass
from pathlib import Path
from typing import AsyncIterable, Mapping, Optional

import httpx
import pytest
from pytest_httpx import HTTPXMock
from quart import Quart

from obs_proxy import utils


@dataclass
class MockRequest:
    body: AsyncIterable
    headers: Mapping


CHUNKS = 16
MEGAZERO = bytes(1024 * 1024)


@pytest.fixture
async def mockrequest(caplog):
    caplog.set_level(logging.DEBUG)
    app = Quart(__name__)
    return app.test_request_context(
        path='/putjob',
        data=MEGAZERO * CHUNKS,
        headers={
            'content-type': 'application/x-unknown',
        },
    )


def find_temp_file(log: str) -> Optional[Path]:
    lines = [line for line in log.splitlines() if 'Buffering into ' in line]
    if not lines:
        return None
    path = Path(lines[0].split()[-1])
    if path.exists():
        print(f"found {path}")
        return path


@pytest.mark.asyncio
async def test_upload_data_no_error(mockrequest, caplog, httpx_mock: HTTPXMock):
    httpx_mock.add_response()

    async with httpx.AsyncClient() as client:
        async with mockrequest:
            resp = await utils.upload_data(
                client, 'http://127.0.0.1/putjob', buffer=True,
            )
            assert resp.status_code == 200
    # successful upload leads to tempfile being deleted
    assert find_temp_file(caplog.text) is None
    assert 'Uploading stream' not in caplog.text
    assert 'e.request = ' not in caplog.text


@pytest.mark.asyncio
async def test_upload_data_req_error(mockrequest, caplog, httpx_mock: HTTPXMock):
    httpx_mock.add_exception(httpx.ReadTimeout("Unable to read within timeout"))

    async with httpx.AsyncClient() as client:
        async with mockrequest:
            with pytest.raises(httpx.ReadTimeout):
                await utils.upload_data(
                    client, 'http://127.0.0.1/putjob', buffer=True,
                )
    # errors need to preserve tempfile
    tempfile = find_temp_file(caplog.text)
    assert tempfile is not None
    assert tempfile.stat().st_size == CHUNKS * len(MEGAZERO)
    assert 'e.request = ' in caplog.text


@pytest.mark.asyncio
async def test_upload_data_no_error_streaming(mockrequest, caplog, httpx_mock: HTTPXMock):
    httpx_mock.add_response(status_code=404)

    async with httpx.AsyncClient() as client:
        async with mockrequest:
            resp = await utils.upload_data(
                client, 'http://127.0.0.1/putjob', buffer=False,
            )
            assert resp.status_code == 404
    # successful upload leads to tempfile being deleted
    assert find_temp_file(caplog.text) is None
    assert 'e.request = ' not in caplog.text
    assert 'Uploading stream' in caplog.text
