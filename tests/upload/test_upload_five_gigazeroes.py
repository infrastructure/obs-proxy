import aiofiles
import httpx
import pytest

from obs_proxy.chunked_uploads import file_size
from obs_proxy.utils import upload_file

mock_worker = """
<worker hostarch="armv8l" ip="172.20.0.3" port="6050" workerid="worker:1">
  <sandbox>chroot</sandbox>
  <linux>
    <version>4.19.0-14</version>
    <flavor>arm64</flavor>
  </linux>
  <hardware>
    <cpu />
    <processors>8</processors>
  </hardware>
</worker>
"""


@pytest.mark.asyncio
async def test_getbinaries(app_testbed):
    client_base = f"http://127.0.0.1:{app_testbed.client_port}"
    async with httpx.AsyncClient(timeout=5 * 60) as client:
        resp = await client.get(
            f"{client_base}/srcserver/worker:1/getbinaries",
        )
        assert resp.content == b"1234567890" * 1024

        resp = await client.get(
            f"{client_base}/repserver/worker:1/getbinaries",
        )
        assert resp.content == b"1234567890" * 1024


@pytest.mark.asyncio
async def test_five_gigazero_upload(app_testbed, tmp_path):
    client_base = f"http://127.0.0.1:{app_testbed.client_port}"
    async with httpx.AsyncClient(timeout=5 * 60) as client:
        await client.post(
            f"{client_base}/worker",
            content=mock_worker,
        )

        fivegig = tmp_path / "fivegig"

        async with aiofiles.open(fivegig, "wb") as f:
            await f.truncate(5 * 1024 * 1024 * 1024)

        size = await file_size(fivegig)

        async with aiofiles.open(fivegig, "rb") as f:
            await upload_file(
                client,
                f"{client_base}/repserver/worker:1/putjob",
                file=f,
                params={
                    'jobid': '62d106cb98d80ec44b18691e6bb5b487',
                },
                headers={
                    "content-length": str(size),
                },
            )
