import aiofiles
import httpx
import pytest

from obs_proxy.chunked_uploads import file_size
from obs_proxy.utils import upload_file


@pytest.fixture
def config_dict():
    return {
        'server': {
            'host': '127.0.0.1',
            'keyfile': 'key.pem',
            'certfile': 'cert.pem',
            'prefix': '/foo',
        },
        'client': {
            'host': '127.0.0.1',
        },
        'workers': {
        },
        'auth': {
            'token': 'token',
        },
        'backend': {
            'host': '127.0.0.1',
        },
    }


mock_worker = """
<worker hostarch="armv8l" ip="172.20.0.3" port="6050" workerid="worker:1">
  <sandbox>chroot</sandbox>
  <linux>
    <version>4.19.0-14</version>
    <flavor>arm64</flavor>
  </linux>
  <hardware>
    <cpu />
    <processors>8</processors>
  </hardware>
</worker>
"""


@pytest.mark.asyncio
async def test_getbinaries(app_testbed):
    client_base = f"http://127.0.0.1:{app_testbed.client_port}"
    async with httpx.AsyncClient(timeout=5 * 60) as client:
        resp = await client.get(
            f"{client_base}/srcserver/worker:1/getbinaries",
        )
        assert resp.content == b"1234567890" * 1024

        resp = await client.get(
            f"{client_base}/repserver/worker:1/getbinaries",
        )
        assert resp.content == b"1234567890" * 1024


@pytest.mark.asyncio
async def test_five_kilozero_upload(app_testbed, tmp_path):
    client_base = f"http://127.0.0.1:{app_testbed.client_port}"
    async with httpx.AsyncClient(timeout=5 * 60) as client:
        await client.post(
            f"{client_base}/worker",
            content=mock_worker,
        )

        fivekilo = tmp_path / "fivekilo"

        async with aiofiles.open(fivekilo, "wb") as f:
            await f.truncate(5 * 1024)

        size = await file_size(fivekilo)

        async with aiofiles.open(fivekilo, "rb") as f:
            await upload_file(
                client,
                f"{client_base}/repserver/worker:1/putjob",
                file=f,
                params={
                    'jobid': '62d106cb98d80ec44b18691e6bb5b487',
                },
                headers={
                    "content-length": str(size),
                },
            )
