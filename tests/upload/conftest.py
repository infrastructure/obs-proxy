import asyncio
import os
import subprocess
import sys
from configparser import ConfigParser
from dataclasses import dataclass
from pathlib import Path

import pytest
from quart import Quart, request


@dataclass
class MockConfig:
    server_port: int
    client_port: int
    worker_port: int
    mock_backend_port: int


@pytest.fixture
def config_dict():
    return {
        'server': {
            'host': '127.0.0.1',
            'keyfile': 'key.pem',
            'certfile': 'cert.pem',
        },
        'client': {
            'host': '127.0.0.1',
        },
        'workers': {
        },
        'debug': {
            'enabled': True,
        },
        'auth': {
            'token': 'token',
        },
        'backend': {
            'host': '127.0.0.1',
        },
    }


@pytest.fixture
def mock_config(tmp_path: Path, unused_tcp_port_factory, config_dict):
    os.environ['TMPDIR'] = os.environ['HOME'] = str(tmp_path)
    config = tmp_path / '.config' / 'obs-proxy' / 'proxy.conf'
    config.parent.mkdir(exist_ok=True, parents=True)

    c = MockConfig(
        server_port=unused_tcp_port_factory(),
        client_port=unused_tcp_port_factory(),
        worker_port=unused_tcp_port_factory(),
        mock_backend_port=unused_tcp_port_factory(),
    )

    cp = ConfigParser()
    cp.read_dict(config_dict)
    cp['server']['port'] = str(c.server_port)
    cp['client']['port'] = str(c.client_port)
    cp['workers']['startport'] = str(c.worker_port)
    cp['workers']['endport'] = str(c.worker_port)
    cp['backend']['srcserver'] = f":{c.mock_backend_port}"
    cp['backend']['repserver'] = f":{c.mock_backend_port}"

    with config.open('w') as f:
        cp.write(f)

    return c


async def capture_stdout_until(p: subprocess.Popen, text: str) -> str:
    out = ""
    while text not in out:
        line = p.stdout.readline()
        out += line
        sys.stdout.write(p.args[-1] + ": " + line)
        await asyncio.sleep(0.1)
    return out


@pytest.fixture
async def spawn_client(mock_config: MockConfig):
    print(f"{mock_config = }")
    p = subprocess.Popen(
        [
            "python3", "-m", "obs_proxy.client",
        ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True,
    )
    await capture_stdout_until(p, "Waiting for the worker")
    yield p, mock_config
    p.terminate()
    p.wait(timeout=5)
    p.kill()
    outs, _ = p.communicate(timeout=1)
    for line in outs.splitlines():
        print(p.args[-1] + ": " + line)


@pytest.fixture
async def spawn_server(mock_config):
    p = subprocess.Popen(
        [
            "python3", "-m", "obs_proxy.server",
        ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True,
    )
    await capture_stdout_until(p, "Will listen on worker ports")
    yield p, mock_config
    p.terminate()
    p.wait(timeout=5)
    p.kill()
    outs, _ = p.communicate(timeout=1)
    for line in outs.splitlines():
        print(p.args[-1] + ": " + line)


async def mock_backend() -> Quart:
    app = Quart(__name__)
    app.config['MAX_CONTENT_LENGTH'] = None

    @app.route('/getbinaries')
    async def mock_getbinaries():
        return "1234567890" * 1024

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>', methods=['POST', 'PUT', 'GET'])
    async def consume(**kv):
        total = 0
        print(request.__dict__)
        async for data in request.body:
            total += len(data)
            print('.', end='')
        print()
        print(f"Consumed {total} bytes.")
        return "ok"

    return app


@pytest.fixture
async def app_testbed(spawn_client, spawn_server):
    _, config = spawn_client

    backend = await mock_backend()

    shutdown_event = asyncio.Event()
    task = asyncio.create_task(
        backend.run_task(
            port=config.mock_backend_port,
            shutdown_trigger=shutdown_event.wait,
        ),
    )
    try:
        yield config
    finally:
        shutdown_event.set()
        await task
        task.cancel()
