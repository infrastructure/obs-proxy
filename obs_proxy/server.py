#!/usr/bin/python3
#
# OBS Proxy: server side
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020-2022 Collabora Ltd
# Copyright (c) 2020-2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import functools
import hashlib
import json
import os
import signal
import ssl
import sys
from pathlib import Path
from typing import Optional

import aiofiles
import httpx
import structlog
import xmltodict
from bidict import bidict
from jsonrpc import Dispatcher
from jsonrpc.jsonrpc2 import JSONRPC20Request, JSONRPC20Response
from quart import (
    Blueprint,
    Quart,
    abort,
    copy_current_websocket_context,
    current_app,
    has_request_context,
    make_response,
    request,
    websocket,
)

from .async_jsonrpc import AsyncJSONRPCResponseManager
from .chunked_uploads import chunked_uploads
from .config import HTTP_TIMEOUT, config
from .rpcqueue import RPCQueue
from .utils import (
    configure_logging,
    filterdict,
    format_ws_request,
    format_ws_response,
    job_alias,
    job_trace,
    open_cache,
    run_multiserver,
    upload_data,
    upload_file,
)
from .worker import ProxiedWorker, Worker

logger = structlog.get_logger()

app = Quart(__name__)
app.config['MAX_CONTENT_LENGTH'] = None

# Blueprint for externally accessible resources
# these will be mounted under a prefix
ext_resources = Blueprint(
    "ext_resources", __name__,
)
ext_resources.register_blueprint(chunked_uploads, url_prefix="/repserver/<path:worker_id>/jobs")


global_client = httpx.AsyncClient(
    timeout=HTTP_TIMEOUT,
    limits=httpx.Limits(max_connections=2048, max_keepalive_connections=256),
)


workers = bidict()

ports = bidict()
port_cache = {}

ws_connections = {}

jobs = bidict()


@app.before_request
@ext_resources.before_request
async def log_port():
    _, port = request.scope['server']
    structlog.contextvars.bind_contextvars(
        http_port=port,
    )


def require_auth(f):
    """Require authentication when accessed over the external server port"""
    @functools.wraps(f)
    async def wrapper(*args, **kwargs):
        ctx = request if has_request_context() else websocket
        _, port = ctx.scope['server']
        if port == config.server.port:
            if not ctx.authorization:
                return 'auth required', 401, {'WWW-Authenticate': 'Basic'}

            # Quart parses auth for us
            if ctx.authorization.type == "basic":
                if (
                    ctx.authorization["username"] != config.auth.username
                    or ctx.authorization["password"] != config.auth.password
                ):
                    logger.debug(f"{ctx.authorization = }, {config.auth = }")
                    abort(403)
            elif ctx.authorization.type == "bearer":
                if ctx.authorization.token != config.auth.token:
                    logger.debug(f"{ctx.authorization = }, {config.auth = }")
                    abort(403)
            else:
                abort(403)

        return await f(*args, **kwargs)

    return wrapper


def handout_port(worker_id):
    """Assign an available port to a worker or return None if no ports are available."""
    if worker_id in port_cache:
        port = port_cache[worker_id]
        if ports[port] is None:
            return port
    available_ports = [port for port in ports.keys() if ports[port] is None]
    return next(iter(available_ports), None)


def find_worker(worker_id):
    """If not set, find the worker ID associated with the port that the request came from."""
    if not worker_id:
        _, port = request.scope['server']
        if not worker_id and ports.get(port):
            worker_id = ports[int(port)].workerid
    known_worker = worker_id in ws_connections
    structlog.contextvars.bind_contextvars(
        worker_id=worker_id,
        known_worker=known_worker,
    )
    return worker_id


def per_worker(f):
    @functools.wraps(f)
    async def wrapper(worker_id: Optional[str] = None, *args, **kwargs):
        worker_id = find_worker(worker_id)
        return await f(worker_id, *args, **kwargs)

    return wrapper


def forget_jobid_mapping(worker_id, job_id):
    known = (worker_id, job_id) in jobs
    if known:
        del jobs[(worker_id, job_id)]
    logger.info(
        "forgetting mapping",
        worker_id=worker_id,
        job_id=job_id,
        known=known,
    )


def map_jobid_to_orig(worker_id, job_id):
    if (worker_id, job_id) in jobs:
        _, orig_job_id = jobs.get((worker_id, job_id))
        logger.info(
            "mapping job",
            worker_id=worker_id,
            job_id=job_id,
            orig_job_id=orig_job_id,
        )
        return orig_job_id
    else:
        logger.warning(
            "not mapping unknown job",
            worker_id=worker_id,
            job_id=job_id,
        )
        return job_id


def map_jobid_to_internal(worker_id, orig_job_id):
    if (worker_id, orig_job_id) in jobs.inverse:
        _, job_id = jobs.inverse.get((worker_id, orig_job_id))
        logger.info(
            "mapping job",
            worker_id=worker_id,
            orig_job_id=orig_job_id,
            job_id=job_id,
        )
        return job_id
    else:
        logger.warning(
            "not mapping unknown job",
            worker_id=worker_id,
            orig_job_id=orig_job_id,
        )
        return orig_job_id


async def worker_ping(worker, state=None, jobid=None):
    w = ProxiedWorker.fromdict(worker)
    if w.workerid not in workers:
        w.externalport = handout_port(w.workerid) or 0
        if not w.externalport:
            logger.debug("no external port?", workers=workers)
        logger.debug("allocated external port", port=w.externalport)
        workers[w.workerid] = w
        ports[w.externalport] = w
        if w.externalport:
            port_cache[w.workerid] = w.externalport
    else:
        workers[w.workerid].update(w)
    workers[w.workerid].state = state or 'idle'

    if jobid:
        orig_job_id, job_id = jobid
        jobs[(w.workerid, job_id)] = (w.workerid, orig_job_id)
    return await post_worker_state(w.workerid, state)


async def post_worker_state(worker_id: str, state: str):
    w = workers[worker_id]
    xml = w.external().asxml(pure=True)
    params = {
        'state': state,
        'arch': w.hostarch,
        'port': w.externalport,
        'workerid': w.workerid,
    }
    if state == 'building':
        logger.info("not updating worker state 'building', OBS does not care", **params)
        return
    logger.info("updating worker state", **params)
    async with httpx.AsyncClient(timeout=HTTP_TIMEOUT) as client:
        try:
            resp = await client.post(
                f"{config.backend.repserver_uri}/worker", params=params, content=xml,
            )
        except httpx.RequestError as e:
            logger.error("update failed", error=e)
            raise
        except httpx.HTTPStatusError as e:
            logger.error("update failed", code=resp.status_code, reason=resp.reason_phrase)
            raise Exception(f"HTTP error: {e}")
        logger.info("updated", code=resp.status_code, reason=resp.reason_phrase)
        return (
            'ok' if resp.status_code == 200 else f"{resp.status_code} {resp.reason_phrase}"
        )


async def job_started(orig_job_id, job_id, worker_id):
    if (worker_id, job_id) in jobs:
        return f"job mapping {worker_id}/{orig_job_id} => {job_id} already known"
    jobs[(worker_id, job_id)] = (worker_id, orig_job_id)
    job_alias(orig_job_id, job_id)
    logger.info(
        "mapping job done",
        worker_id=worker_id,
        orig_job_id=orig_job_id,
        job_id=job_id,
    )
    return 'ok'


@app.route('/ping')
async def ping():
    logger.info("ping")
    return "All OK", 200


@app.route('/worker/<path:worker_id>/config')
@require_auth
async def dump_config(worker_id=None):
    return filterdict(config.as_dict(), ['backend'])


@app.route('/logfile')
@ext_resources.route('/worker/<path:worker_id>/logfile', methods=['GET'])
@require_auth
@per_worker
async def logfile(worker_id: str = None):
    worker_id = find_worker(worker_id)
    if worker_id not in ws_connections:
        return 'not found', 404
    job_id = request.args.get('jobid', None)
    if job_id:
        job_id = map_jobid_to_internal(worker_id, job_id)
    arg_start = request.args.get('start', None)
    arg_end = request.args.get('end', None)
    arg_view = request.args.get('view', None)
    params = {'jobid': job_id, 'worker_id': worker_id}
    if arg_start:
        params['start'] = arg_start
    if arg_end:
        params['end'] = arg_end
    if arg_view:
        params['view'] = arg_view
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='worker_log',
            params=params,
        ),
    )
    if resp.error:
        return resp.error, 500
    else:
        return resp.result.get('content', ''), resp.result['code']


@ext_resources.route('/worker/<path:worker_id>/ping', methods=['POST'])
@require_auth
async def worker_ping_http(worker_id: str, state=None):
    data = await request.get_json()
    return worker_ping(data, state)


@ext_resources.websocket('/worker/<path:worker_id>/events')
@require_auth
async def worker_events_ws(worker_id: str):
    queue = RPCQueue(name=worker_id)

    async def scoped_worker_ping(**kwargs):
        new_worker_id = kwargs.pop('worker_id', None)
        if new_worker_id and new_worker_id != worker_id:
            ws_connections[new_worker_id] = queue
            logger.info("new worker", worker_id=new_worker_id, shared_with=worker_id)
        return await worker_ping(**kwargs)

    dispatcher = Dispatcher()
    dispatcher.add_method(job_started)
    dispatcher.add_method(scoped_worker_ping, name="worker_ping")

    @copy_current_websocket_context
    async def send(queue):
        while True:
            message = await queue.get()
            await websocket.send(message.json)

    @copy_current_websocket_context
    async def recv(queue):
        while True:
            logger.info("... awaiting")
            data = await websocket.receive()
            try:
                message = json.loads(data)
            except (TypeError, ValueError):
                logger.error(f"### invalid JSON of length {len(data)}", json=data)
                continue
            if 'id' in message and ('result' in message or 'error' in message):
                logger.info(">>>", kind="call.result", **format_ws_response(message))
                response = JSONRPC20Response(_id=message['id'], **message)
                await queue.reply(response)
            elif 'method' in message:
                logger.info(">>>", kind="call", **format_ws_request(message))
                response = await AsyncJSONRPCResponseManager.handle(data, dispatcher)
                logger.info("<x-", kind="call.result.unsent", **format_ws_response(response.data))

    logger.info(f"(o) {worker_id} online")
    ws_connections[worker_id] = queue
    try:
        producer = asyncio.create_task(send(queue))
        consumer = asyncio.create_task(recv(queue))
        await asyncio.gather(producer, consumer)
    finally:
        affected_worker_ids = [k for k, v in ws_connections.items() if v == queue]
        for affected_worker_id in affected_worker_ids:
            del ws_connections[affected_worker_id]


@ext_resources.route('/worker/<path:worker_id>/events')
@require_auth
async def worker_events(worker_id: str):
    return "Use WebSockets", 500


@app.route('/info')
@ext_resources.route('/worker/<path:worker_id>/buildinfo')
@require_auth
@per_worker
async def worker_buildinfo(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    params = dict(request.args)
    orig_job_id = params.get('jobid', None)
    params['jobid'] = map_jobid_to_internal(worker_id, orig_job_id)

    params['worker_id'] = worker_id
    params['action'] = 'info'
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='worker_action',
            params=params,
        ),
    )
    if resp.error:
        return resp.error, 500
    return resp.result.get('content', ''), resp.result['code']


@ext_resources.route('/worker/<path:worker_id>/state', methods=['GET', 'PUT'])
@require_auth
@per_worker
async def worker_state(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    if request.method == 'PUT':
        workers[worker_id].state = await request.data
        return await post_worker_state(worker_id, workers[worker_id].state)
    else:
        return workers[worker_id].state


async def update_worker_info(params):
    worker_id = params['worker_id']
    orig_job_id = params.get('jobid', None)
    params['jobid'] = map_jobid_to_internal(worker_id, orig_job_id)

    job_trace("server", orig_job_id, worker_id, f"=> info {params['jobid']}")
    logger.debug("=> info", params=params)
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='worker_info',
            params=params,
        ),
    )
    job_trace("server", orig_job_id, worker_id, f"<= {resp.data}")
    if resp.error:
        logger.error("error in worker update", error=resp.error)
        return
    w = Worker.fromdict(resp.result['worker'])
    logger.debug("updating worker", info=w)
    workers[worker_id].update(w)


@app.route('/worker')
@ext_resources.route('/worker/<path:worker_id>/info')
@require_auth
@per_worker
async def worker_info(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    params = dict(request.args)

    if worker_id not in workers:
        logger.warning("worker in ws_connections but not in workers?!")
        return 'not found', 404

    params['worker_id'] = worker_id

    logger.debug("updating worker in background", params=params)
    app.add_background_task(update_worker_info, params)

    w = workers[worker_id]
    return w.external().asxml(pure=True, meta=False), {'content-type': 'text/xml'}


@app.route('/worker/<path:worker_id>/status')
@require_auth
@per_worker
async def worker_status(worker_id: str = None):
    return workers[worker_id].asdict() if worker_id in workers else ('not found', 404)


@app.route('/worker/<path:worker_id>/logfile', methods=['POST'])
@require_auth
@per_worker
async def post_logfile(worker_id: str = None):
    logger.debug(f"{request.host = }")
    logger.debug(f"{request.args = }")
    return 'ok'


@app.route('/kill')
@app.route('/discard')
@app.route('/badhost')
@app.route('/sysrq')
@app.route('/worker/<path:worker_id>/kill')
@app.route('/worker/<path:worker_id>/discard')
@app.route('/worker/<path:worker_id>/badhost')
@app.route('/worker/<path:worker_id>/sysrq')
@require_auth
@per_worker
async def get_action(worker_id: str):
    worker_id = find_worker(worker_id)
    if worker_id not in ws_connections:
        return 'not found', 404
    job_id = request.args.get('jobid', None)
    if job_id:
        job_id = map_jobid_to_internal(worker_id, job_id)
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='worker_action',
            params={'action': request.path.split('/')[-1], 'jobid': job_id, 'worker_id': worker_id},
        ),
    )
    if resp.error:
        return resp.error, 500
    else:
        return resp.result.get('content', ''), resp.result['code']


@ext_resources.route('/repserver/<path:worker_id>/putjob', methods=['POST'])
@ext_resources.route('/repserver/<path:worker_id>/workerdispatched', methods=['POST'])
@require_auth
async def worker_putjob(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    params = dict(request.args)
    job_id = params['jobid']
    params['jobid'] = map_jobid_to_orig(worker_id, job_id)

    job_trace("server", params['jobid'], worker_id, f"<= putjob {job_id}")
    uri = proxied_uri()

    async with httpx.AsyncClient(timeout=HTTP_TIMEOUT) as client:
        try:
            chunked = params.pop("chunked_upload", False)
            if chunked:
                spooled = Path(current_app.spool_dir) / worker_id / job_id
                async with aiofiles.open(spooled, "rb") as f:
                    resp = await upload_file(
                        client, uri, file=f, params=params,
                    )
                spooled.unlink(missing_ok=True)
            else:
                resp = await upload_data(
                    client, uri, config.server.buffer_uploads, params=params,
                )
        except httpx.HTTPError as e:
            job_trace("server", params['jobid'], worker_id, f"<= {e}")
            logger.exception(" !  putjob")
            return str(e), 500
        logger.info(f"    putjob {resp = }")
        logger.info(resp.content)
        job_trace("server", params['jobid'], worker_id, f"<= {resp.status_code} {resp.reason_phrase}")
        job_trace("server", params['jobid'], worker_id, f"content:\n{resp.content}")
        if resp.is_success:
            forget_jobid_mapping(worker_id, job_id)
        return resp.content, resp.status_code


@app.route('/build', methods=['PUT'])
@ext_resources.route('/worker/<path:worker_id>/build', methods=['PUT'])
@require_auth
@per_worker
async def worker_build(worker_id: str = None):
    if worker_id not in ws_connections:
        return 'not found', 404
    job_xml = await request.get_data()
    job_id = hashlib.md5(job_xml).hexdigest()
    job_data = xmltodict.parse(job_xml)
    job_trace("server", job_id, worker_id, "=> build job:")
    job_trace("server", job_id, worker_id, job_data)
    resp = await ws_connections[worker_id].call(
        JSONRPC20Request(
            method='submit_job',
            params={'extra': request.args, 'job': job_data, 'jobid': job_id, 'worker_id': worker_id},
        ),
    )
    job_trace("server", job_id, worker_id, f"<= {resp.data}")
    if resp.error:
        return resp.error, 500
    else:
        return resp.result.get('content', ''), resp.result['code']


def proxied_uri():
    """
    Construct a URI of the backend's resource corresponding to the proxy's URI

    >>> config.backend.repserver_uri = "http://repserver:1234"
    >>> config.backend.srcserver_uri = "http://srcserver:5678"
    >>> async def test_app_context(uri):
    ...     async with app.test_request_context(uri, method="GET"):
    ...         return proxied_uri()
    >>> config.server.prefix = ""
    >>> asyncio.run(test_app_context('/repserver/worker:0/badpackagebinaryversionlist'))
    'http://repserver:1234/badpackagebinaryversionlist'
    >>> asyncio.run(test_app_context('/srcserver/worker:0/badpackagebinaryversionlist'))
    'http://srcserver:5678/badpackagebinaryversionlist'
    >>> asyncio.run(test_app_context('/repserver/worker:1/build/project/repository/arch/package'))
    'http://repserver:1234/build/project/repository/arch/package'
    >>> asyncio.run(test_app_context('/srcserver/worker:1/source/project/package'))
    'http://srcserver:5678/source/project/package'
    >>> config.server.prefix = "/foobar"
    >>> asyncio.run(test_app_context('/foobar/repserver/worker:0/badpackagebinaryversionlist'))
    'http://repserver:1234/badpackagebinaryversionlist'
    >>> asyncio.run(test_app_context('/foobar/srcserver/worker:0/badpackagebinaryversionlist'))
    'http://srcserver:5678/badpackagebinaryversionlist'
    >>> asyncio.run(test_app_context('/foobar/repserver/worker:1/build/project/repository/arch/package'))
    'http://repserver:1234/build/project/repository/arch/package'
    >>> asyncio.run(test_app_context('/foobar/srcserver/worker:1/source/project/package'))
    'http://srcserver:5678/source/project/package'
    """
    path = request.path.split(config.server.prefix, 1)[1] if config.server.prefix else request.path
    _, backend, worker_id, *args = path.split('/')
    backend_uri = config.backend.repserver_uri if backend == "repserver" else config.backend.srcserver_uri
    return backend_uri + '/' + '/'.join(path.split('/')[3:])


@ext_resources.route('/repserver/<path:worker_id>/badpackagebinaryversionlist')
@ext_resources.route('/repserver/<path:worker_id>/getbinaries')
@ext_resources.route('/repserver/<path:worker_id>/getbinaryversions')
@ext_resources.route('/repserver/<path:worker_id>/getbuildcode')
@ext_resources.route('/repserver/<path:worker_id>/getjobdata')
@ext_resources.route('/repserver/<path:worker_id>/getpackagebinaryversionlist')
@ext_resources.route('/repserver/<path:worker_id>/getpreinstallimageinfos')
@ext_resources.route('/repserver/<path:worker_id>/getworkercode')
@ext_resources.route('/repserver/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@require_auth
async def proxy_rep_request(worker_id: str, **kv):
    uri = proxied_uri()
    logger.info(f"Proxying {uri}")

    async def async_generator(response):
        try:
            async for chunk in response.aiter_bytes():
                yield chunk
        finally:
            await response.aclose()

    req = global_client.build_request('GET', uri, params=request.args)
    try:
        resp = await global_client.send(req, stream=True)
    except httpx.HTTPError as e:
        return str(e), 500
    response = await make_response(
        async_generator(resp),
        resp.status_code,
        filterdict(resp.headers, ['content-type', 'content-length', 'cache-control']),
    )
    response.timeout = HTTP_TIMEOUT
    return response


@ext_resources.route('/srcserver/<path:worker_id>/getbinaries')
@ext_resources.route('/srcserver/<path:worker_id>/getbinaryversions')
@ext_resources.route('/srcserver/<path:worker_id>/getconfig')
@ext_resources.route('/srcserver/<path:worker_id>/getobsgendiffdata')
@ext_resources.route('/srcserver/<path:worker_id>/getsources')
@ext_resources.route('/srcserver/<path:worker_id>/getsslcert')
@ext_resources.route('/srcserver/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@ext_resources.route('/srcserver/<path:worker_id>/source/<project>')
@ext_resources.route('/srcserver/<path:worker_id>/source/<project>/<package>')
@require_auth
async def proxy_src_request(worker_id: str, **kv):
    uri = proxied_uri()
    logger.info(f"Proxying {uri}")

    async def async_generator(response):
        try:
            async for chunk in response.aiter_bytes():
                yield chunk
        finally:
            await response.aclose()

    req = global_client.build_request('GET', uri, params=request.args)
    try:
        resp = await global_client.send(req, stream=True)
    except httpx.HTTPError as e:
        logger.exception(f"=!= HTTP error for GET {uri}:")
        return str(e), 500
    response = await make_response(
        async_generator(resp),
        resp.status_code,
        filterdict(resp.headers, ['content-type', 'content-length', 'cache-control']),
    )
    response.timeout = HTTP_TIMEOUT
    return response


def run():
    configure_logging()

    try:
        port_cache = open_cache("port_cache")
        logger.info(f"Using disk cache: {port_cache.path}")
    except OSError as e:
        logger.warning(f"Running without a disk cache: {e}")

    global ports

    ports = {port: None for port in config.worker_ports}

    logger.info(f"Proxying for srcserver {config.backend.srcserver_uri} and repserver {config.backend.repserver_uri}")

    loop = asyncio.get_event_loop()
    shutdown_event = asyncio.Event()

    def _terminate():
        logger.info("Terminating.")
        os._exit(0)

    def _signal_handler(*e):
        shutdown_event.set()
        logger.info("Received a signal, will terminate")
        loop.call_later(1, _terminate)

    def _exception_handler(loop: asyncio.AbstractEventLoop, context: dict) -> None:
        exception = context.get("exception")
        if isinstance(exception, ssl.SSLError):
            pass  # Handshake failure
        else:
            loop.default_exception_handler(context)

    try:
        loop.add_signal_handler(signal.SIGTERM, _signal_handler)
        loop.add_signal_handler(signal.SIGINT, _signal_handler)
        loop.set_exception_handler(_exception_handler)
        pass
    except (AttributeError, NotImplementedError):
        pass

    app.register_blueprint(ext_resources, url_prefix=config.server.prefix)

    if config.server.tls:
        worker = run_multiserver(
            app,
            host=config.server.host,
            https_ports=[config.server.port],
            ports=[*ports.keys()],
            shutdown_trigger=shutdown_event.wait,
            debug=False,
            certfile=config.server.certfile,
            keyfile=config.server.keyfile,
        )
    else:
        worker = run_multiserver(
            app,
            host=config.server.host,
            https_ports=[],
            ports=[config.server.port, *ports.keys()],
            shutdown_trigger=shutdown_event.wait,
            debug=False,
        )
    logger.info(f"Will listen on worker ports {config.worker_ports[0]} to {config.worker_ports[-1]}")

    try:
        loop.run_until_complete(worker)
    except Exception:  # noqa: B902
        logger.exception("Caught an exception:")
        sys.exit(1)
    finally:
        try:
            tasks = [task for task in asyncio.all_tasks(loop) if not task.done()]
            logger.debug(f"{tasks = }")
            if tasks:
                for task in tasks:
                    task.cancel()
                loop.run_until_complete(
                    asyncio.gather(*tasks, return_exceptions=True),
                )
                loop.run_until_complete(loop.shutdown_asyncgens())
        finally:
            asyncio.set_event_loop(None)
            loop.close()


if __name__ == '__main__':
    run()
