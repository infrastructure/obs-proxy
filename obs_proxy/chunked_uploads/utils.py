# Misc utilities
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2022 Collabora Ltd
# Copyright (c) 2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import hashlib
import os
import re

import aiofiles
import aiofiles.os

md5_re = re.compile(r"[A-Fa-f0-9]{32}")


def is_valid_md5(jobid: str) -> bool:
    return md5_re.match(jobid) is not None


HASH_CHUNK_SIZE = 64 * 1024


async def hash_file(path: os.PathLike) -> str:
    file_hasher = hashlib.md5()
    async with aiofiles.open(path, "rb") as f:
        while chunk := await f.read(HASH_CHUNK_SIZE):
            file_hasher.update(chunk)

    return file_hasher.hexdigest()


async def file_size(path: os.PathLike | str) -> int:
    return await aiofiles.os.path.getsize(path)
