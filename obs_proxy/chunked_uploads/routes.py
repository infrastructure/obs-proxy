# Chunked uploads server-side blueprint
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2022 Collabora Ltd
# Copyright (c) 2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import tempfile
from logging import debug
from pathlib import Path

import aiofiles
from quart import current_app, make_response, request
from werkzeug.datastructures import ContentRange
from werkzeug.exceptions import (
    BadRequest,
    LengthRequired,
    NotFound,
    RequestEntityTooLarge,
)
from werkzeug.http import parse_content_range_header

from . import chunked_uploads
from .utils import file_size, hash_file, is_valid_md5


@chunked_uploads.before_app_first_request
def create_spool_dir():
    if not hasattr(current_app, 'spool_dir'):
        current_app.spool_dir = Path(tempfile.gettempdir()) / "obs-proxy-spool"

    current_app.spool_dir.mkdir(parents=True, exist_ok=True)


@chunked_uploads.route("/")
async def root():
    return "ok"


@chunked_uploads.url_value_preprocessor
def validate_params(endpoint, values):
    jobid = values.get("jobid", "")
    if not is_valid_md5(jobid):
        raise BadRequest("incorrect jobid")

    content_range = request.headers.get('Patch-Content-Range')
    if request.method == 'PATCH':
        content_range = parse_content_range_header(content_range)
        if not content_range:
            raise BadRequest("A range is required")
        if content_range.start is None:
            raise BadRequest("The range must have a start")
        if content_range.units != 'bytes':
            raise BadRequest(f"Unsupported unit {content_range.units}")

        values['content_range'] = content_range

        if request.content_length is None:
            raise LengthRequired()

        if request.content_length != content_range.stop - content_range.start:
            raise BadRequest("Content length must match the range")


@chunked_uploads.route("/<path:jobid>", methods=['PATCH'])
async def patch(worker_id: str, jobid: str, content_range: ContentRange):
    per_worker_dir = Path(current_app.spool_dir) / worker_id
    per_worker_dir.mkdir(exist_ok=True)
    (per_worker_dir / jobid).touch(exist_ok=True)
    async with aiofiles.open(per_worker_dir / jobid, "r+b") as spooled:
        await spooled.seek(content_range.start)
        async for data in request.body:
            await spooled.write(data)

        pos = await spooled.tell()

    if pos == content_range.stop:
        return f"ok, wrote from {content_range.start} to {content_range.stop}"
    else:
        debug(f"{request.headers = }, incomplete partial upload: {pos} != {content_range.stop}")
        raise RequestEntityTooLarge(f"Payload does not match the patched range: {pos} != {content_range.stop}")


@chunked_uploads.route("/<path:jobid>", methods=['HEAD'])
async def verify_file(worker_id: str, jobid: str):
    spooled_file = Path(current_app.spool_dir) / worker_id / jobid
    if not spooled_file.is_file():
        raise NotFound()

    file_hash = await hash_file(spooled_file)
    length = await file_size(spooled_file)

    response = await make_response('', 200)
    response.set_etag(file_hash)
    response.content_length = length
    return response
