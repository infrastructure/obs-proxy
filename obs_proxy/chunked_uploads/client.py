# Chunked uploads client
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2022 Collabora Ltd
# Copyright (c) 2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import hashlib
from dataclasses import dataclass

import httpx
import structlog
from aiofiles.threadpool import AsyncFileIO
from werkzeug.datastructures import ContentRange
from werkzeug.http import unquote_etag

logger = structlog.get_logger()

UPLOAD_CHUNK_SIZE = 512 * 1024 * 1024
MAX_ATTEMPTS = 5


@dataclass(frozen=True)
class ChunkedUploadError(Exception):
    content_range: ContentRange
    attempt: int

    def __str__(self):
        return f"Upload error at range {self.content_range} (attempt {self.attempt}): {self.__cause__}"


@dataclass(frozen=True)
class ChunkedUploadVerificationError(Exception):
    expected: str
    actual: str

    def __str__(self):
        return f"Uploaded file doesn't match the local file (expected: {self.expected}, actual: {self.actual})"


async def upload_chunked(
        client: httpx.AsyncClient,
        uri: str,
        data: AsyncFileIO,
        size: int,
        chunk_size: int = UPLOAD_CHUNK_SIZE,
        headers=None,
        **kv,
):
    if not headers:
        headers = {}

    content_range = None
    attempt = 0
    file_hasher = hashlib.md5()
    try:
        chunk_headers = headers.copy()
        await data.seek(0)
        while True:
            offset = await data.tell()
            chunk = await data.read(chunk_size)
            if not chunk:
                break
            pos = await data.tell()

            file_hasher.update(chunk)

            content_range = ContentRange(
                start=offset,
                stop=pos,
                length=size,
                units="bytes",
            )
            chunk_headers["Patch-Content-Range"] = content_range.to_header()
            chunk_headers["Content-Length"] = str(len(chunk))
            exc = None
            for attempt in range(MAX_ATTEMPTS):
                try:
                    logger.debug("patching", uri=uri, size=len(chunk), content_range=content_range, attempt=attempt)
                    r = await client.patch(
                        uri,
                        content=chunk,
                        headers=chunk_headers,
                        **kv,
                    )
                    logger.debug("patch result", result=r)
                    if r.is_success:
                        break
                    else:
                        r.raise_for_status()
                except httpx.HTTPStatusError as e:
                    if not e.response.is_server_error:
                        logger.debug("not server error, not retrying", resp=e.response)
                        raise
                    logger.exception("retrying", attempt=attempt)
                    exc = e
                except httpx.HTTPError as e:
                    logger.exception("retrying", attempt=attempt)
                    exc = e
            else:
                raise exc

        logger.debug("get etag", uri=uri)
        # now verify the hash
        r = await client.head(
            uri,
            headers=headers,
        )
        calculated_hash = file_hasher.hexdigest()
        etag, _ = unquote_etag(r.headers.get("etag"))

        logger.debug("check upload", etag=etag, calculated_hash=calculated_hash)

        if etag != calculated_hash:
            raise ChunkedUploadVerificationError(
                expected=calculated_hash,
                actual=etag,
            )

    except httpx.HTTPError as e:
        raise ChunkedUploadError(
            content_range=content_range,
            attempt=attempt,
        ) from e
