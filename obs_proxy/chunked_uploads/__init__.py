from quart import Blueprint

chunked_uploads = Blueprint(
    "chunked_uploads", __name__,
)


from .routes import *  # noqa: E402,F401,F403
