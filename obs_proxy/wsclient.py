#!/usr/bin/python3
#
# OBS Proxy: client side
# WebSocket client and related handling
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020-2022 Collabora Ltd
# Copyright (c) 2020-2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import hashlib
import json
import random
import ssl

import httpx
import structlog
import websockets
import xmltodict
from hypercorn.utils import raise_shutdown
from jsonrpc.jsonrpc import JSONRPC20Request

from .async_jsonrpc import AsyncJSONRPCResponseManager, dispatcher
from .config import HTTP_TIMEOUT, config
from .utils import job_alias, job_trace, print_ws_message
from .worker import Worker

random.seed()

logger = structlog.get_logger()

queue = asyncio.Queue()

client = httpx.AsyncClient(
    http2=False,
    timeout=HTTP_TIMEOUT,
    limits=httpx.Limits(max_connections=2048, max_keepalive_connections=256),
)

jobs = {}

# Set upon a first connect
workers = {}

# Raised when the worker connects for the first time.
# The worker tells the proxy client its name so that
# it can we used when establishing the WebSocket link.
first_connect = asyncio.Event()


def worker_uri(worker, action):
    return f'http://{worker.ip}:{worker.port}/{action}'


def server_uri(path='', ws=False, auth=True, worker_id=None):
    if config.auth.username and auth:
        auth_prefix = f"{config.auth.username}:{config.auth.password}@"
    else:
        auth_prefix = ""
    if worker_id is None:
        worker_id = '-'
    if ws:
        scheme = 'wss' if config.server.tls else 'ws'
    else:
        scheme = 'https' if config.server.tls else 'http'
    return f'{scheme}://{auth_prefix}{config.server.host}:{config.server.port}{config.server.prefix}/worker/{worker_id}{path}'


@dispatcher.add_method
async def worker_log(jobid=None, worker_id=None, **args):
    global workers
    if worker_id and worker_id in workers:
        worker = workers[worker_id]
        uri = worker_uri(worker, 'logfile')
        try:
            logger.info(
                f"==> [log {args.get('start', '-')}:{args.get('end', '-')} {args.get('view', 'log')}]",
                job_id=jobid,
                worker_id=worker_id,
                known=jobs.get(worker_id),
            )
            args['nostream'] = 1
            if jobid:
                args['jobid'] = jobid
            resp = await client.get(uri, params=args)
        except httpx.ReadTimeout:
            logger.info("    (timed out)", job_id=jobid, worker_id=worker_id)
            return {'content': '', 'code': 200}
        except httpx.HTTPError:
            raise
        logger.info(f"<== [{resp.status_code} {resp.reason_phrase}]", job_id=jobid, worker_id=worker_id)
        return {'content': resp.text, 'code': resp.status_code}
    else:
        raise Exception(
            "No worker", {
                'action': 'logfile',
                'jobid': jobid,
                'worker_id': worker_id,
                'workers': list(workers.keys()),
            },
        )


@dispatcher.add_method
async def worker_action(action=None, jobid=None, worker_id=None):
    global workers
    if worker_id and worker_id in workers:
        worker = workers[worker_id]
        if worker_id in jobs:
            orig_job_id, new_job_id = jobs[worker_id]
            if orig_job_id == jobid:
                logger.warn("found unmapped jobid!", orig_job_id=orig_job_id, new_job_id=new_job_id)
        uri = worker_uri(worker, action)
        try:
            logger.info(
                f"==> [{action}]",
                job_id=jobid,
                worker_id=worker_id,
                known=jobs.get(worker_id),
            )
            job_trace("client", jobid, worker_id, f"=> {action}")
            resp = await client.get(uri, params={'jobid': jobid} if jobid else None)
        except httpx.HTTPError as e:
            logger.exception(f"=!> [{action}]", job_id=jobid, worker_id=worker_id)
            job_trace("client", jobid, worker_id, f"<= {e}")
            raise
        job_trace("client", jobid, worker_id, f"<= {resp.status_code} {resp.reason_phrase}")
        logger.info(f"<== [{resp.status_code} {resp.reason_phrase}]", job_id=jobid, worker_id=worker_id)
        return {'content': resp.text, 'code': resp.status_code}
    else:
        job_trace("client", jobid, worker_id, "<= [no such worker]")
        raise Exception(
            "No worker", {
                'action': action,
                'jobid': jobid,
                'worker_id': worker_id,
                'workers': list(workers.keys()),
            },
        )


@dispatcher.add_method
async def worker_info(jobid=None, worker_id=None):
    from .repserver import workers
    if worker_id and worker_id in workers:
        worker = workers[worker_id]
        if worker_id in jobs:
            orig_job_id, new_job_id = jobs[worker_id]
            if orig_job_id == jobid:
                logger.warn("found unmapped jobid!", orig_job_id=orig_job_id, new_job_id=new_job_id)

        action = 'worker'
        uri = worker_uri(worker, action)
        try:
            logger.info(
                f"==> [{action}]",
                job_id=jobid,
                worker_id=worker_id,
                known=jobs.get(worker_id),
            )
            job_trace("client", jobid, worker_id, f"=> {action}")
            resp = await client.get(uri, params={'jobid': jobid} if jobid else None)
        except httpx.HTTPError as e:
            logger.exception(f"=!> [{action}]", job_id=jobid, worker_id=worker_id)
            job_trace("client", jobid, worker_id, f"<= {e}")
            raise
        job_trace("client", jobid, worker_id, f"<= {resp.status_code} {resp.reason_phrase}")
        if resp.status_code == 200:
            # update cached data
            w = Worker.fromxml(resp.text)
            worker.update(w)
            logger.info(f"<== [{resp.status_code} {resp.reason_phrase}]", job_id=jobid, worker_id=worker_id)
            return {
                'worker': worker.asdict(),
                'code': resp.status_code,
                'reason': resp.reason_phrase,
            }
        else:
            logger.info(f"<== [{resp.status_code} {resp.reason_phrase} {resp.text}]", job_id=jobid, worker_id=worker_id)
            return {
                'worker': worker.asdict(),
                'code': resp.status_code,
                'content': resp.text,
                'reason': resp.reason_phrase,
            }
    else:
        job_trace("client", jobid, worker_id, "<= [no such worker]")
        raise Exception(
            "No worker", {
                'action': 'worker',
                'jobid': jobid,
                'worker_id': worker_id,
                'workers': list(workers.keys()),
            },
        )


@dispatcher.add_method
async def submit_job(job=None, jobid=None, worker_id=None, extra=None):
    from .repserver import workers
    if worker_id and worker_id in workers:
        worker = workers[worker_id]
        uri = worker_uri(worker, 'build')
        notification = None
        new_jobid = None
        if not extra:
            extra = {}
        try:
            extra.update({'port': config.client.port})
            job['buildinfo'].update({
                "@srcserver": f"http://{config.client.host}:{config.client.port}/srcserver/{worker.workerid}",
                "@reposerver": f"http://{config.client.host}:{config.client.port}/repserver/{worker.workerid}",
            })
            for path in job['buildinfo']['path']:
                if path['@server'] == config.backend.srcserver_uri:
                    path['@server'] = f"http://{config.client.host}:{config.client.port}/srcserver/{worker.workerid}"
                if path['@server'] == config.backend.repserver_uri:
                    path['@server'] = f"http://{config.client.host}:{config.client.port}/repserver/{worker.workerid}"

            job_xml = xmltodict.unparse(job).encode('utf-8')
            job_trace("client", jobid, worker_id, "=> submit_job")
            resp = await client.put(uri, params=extra, content=job_xml)
            if jobid:
                new_jobid = hashlib.md5(job_xml).hexdigest()
                logger.info(f"==> [build {worker_id}/{jobid} => {new_jobid}]", orig_job_id=jobid, new_job_id=new_jobid, worker_id=worker_id)

                notification = JSONRPC20Request(
                    method='job_started',
                    params={'orig_job_id': jobid, 'job_id': new_jobid, 'worker_id': worker_id},
                )
                job_alias(jobid, new_jobid)
        except httpx.HTTPError as e:
            logger.exception("submit_job", orig_job_id=jobid, new_job_id=new_jobid, worker_id=worker_id)
            job_trace("client", jobid, worker_id, f"<= {e}")
            raise Exception(f"HTTP error: {e}", uri)
        job_trace("client", jobid, worker_id, f"<= {resp.status_code} {resp.reason_phrase}")
        logger.info(f"<== [{resp.status_code} {resp.reason_phrase}]", orig_job_id=jobid, new_job_id=new_jobid, worker_id=worker_id)
        if resp.status_code == 200 and notification:
            queue.put_nowait(notification)
            jobs[worker_id] = (jobid, new_jobid)

        return {'content': resp.text, 'code': resp.status_code}
    else:
        job_trace("client", jobid, worker_id, "<= [no such worker]")
        raise Exception(
            "No worker", {
                'action': 'submit_job',
                'jobid': jobid,
                'worker_id': worker_id,
                'job': job,
                'extra': extra,
                'workers': list(workers.keys()),
            },
        )


async def consumer_handler(websocket):
    async for data in websocket:
        try:
            message = json.loads(data)
        except (TypeError, ValueError):
            logger.error(f"### invalid JSON of length {len(data)}")
            continue
        try:
            if 'method' in message:
                print_ws_message('>>>', message)
                response = await AsyncJSONRPCResponseManager.handle(message, dispatcher)
                print_ws_message('<<-', response)
                await websocket.send(response.json)
            else:
                logger.error(f"### Unknown message: {message = }")
        except Exception:  # noqa: B902
            logger.exception("### Caught exception:")


async def producer_handler(websocket):
    while True:
        message = await queue.get()
        print_ws_message('<<<', message)
        await websocket.send(message.json)


async def get_worker_state(worker_id: str, jobid=None):
    try:
        resp = await worker_action(action='info', jobid=jobid, worker_id=worker_id)
        job_xml = resp['content']
        job = xmltodict.parse(job_xml)
        if 'error' in job['buildinfo']:
            return job['buildinfo']['error']
        else:
            return 'building'
    except httpx.HTTPError:
        logger.exception(f"Caught exception while querying {worker_id}:")
        return None


async def wsclient(queue, shutdown_trigger=None):
    global workers, first_connect

    logger.info("Waiting for the worker to connect")
    await first_connect.wait()

    worker = list(workers.values())[0]

    if config.auth.username:
        extra_headers = {}
        explanation = f", authenticated with a password as {config.auth.username}"
    else:
        extra_headers = {"Authorization": f"Bearer {config.auth.token}"}
        explanation = ", authenticated with a token"
    uri = server_uri(path='/events', ws=True, auth=True, worker_id=worker.workerid)
    while True:
        timeout = random.randrange(2, 10)
        try:
            if config.server.insecure:
                ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
                ssl_ctx.check_hostname = False
                ssl_ctx.verify_mode = ssl.CERT_NONE
            elif config.server.tls:
                ssl_ctx = True
            else:
                ssl_ctx = None
            async with websockets.connect(
                uri,
                compression=None,
                extra_headers=extra_headers,
                ssl=ssl_ctx,
            ) as websocket:
                unauth_uri = server_uri(path='/events', ws=True, auth=False, worker_id=worker.workerid)
                logger.info(f"Connected to {unauth_uri} using WebSockets{explanation}")
                if workers and not first_connect.is_set():
                    logger.info("Bootstrapping the workers")
                    for w in workers:
                        state = await get_worker_state(w)
                        queue.put_nowait(
                            JSONRPC20Request(
                                method='worker_ping',
                                params={
                                    'worker': workers[w].asdict(),
                                    'state': state,
                                    'worker_id': w,
                                    'jobid': jobs.get(w),
                                },
                            ),
                        )
                consumer_task = asyncio.ensure_future(consumer_handler(websocket))
                producer_task = asyncio.ensure_future(producer_handler(websocket))
                shutdown_task = asyncio.create_task(raise_shutdown(shutdown_trigger))
                done, pending = await asyncio.wait(
                    [consumer_task, producer_task, shutdown_task],
                    return_when=asyncio.FIRST_COMPLETED,
                )
                for task in [consumer_task, producer_task]:
                    task.cancel()
                if shutdown_task in done:
                    return
        except (websockets.exceptions.WebSocketException, OSError):
            logger.exception("Caught an exception")
            logger.info(f"Waiting for {timeout} seconds before reconnecting")
            await asyncio.sleep(timeout)
            first_connect.clear()
            continue
