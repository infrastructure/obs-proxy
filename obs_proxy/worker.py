# OBS Proxy: worker dataclasses
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020-2022 Collabora Ltd
# Copyright (c) 2020-2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
from dataclasses import asdict, dataclass, field, fields

import xmltodict


@dataclass
class Worker:
    port: int
    hostarch: str
    ip: str = None
    meta: dict = field(default_factory=dict)
    workerid: str = None

    def __post_init__(self):
        if not self.workerid:
            self.workerid = f"{self.ip}:{self.port}"
        try:
            self.port = int(self.port)
        except ValueError:
            self.port = 0

    def __hash__(self):
        return hash(self.workerid)

    def asxml(self, pure=False, meta=True):
        """Return an XML representation of a worker object."""
        meta = self.meta.copy() if meta else {}
        meta.update({
            '@port': self.port,
            '@workerid': self.workerid,
            '@hostarch': self.hostarch,
        } if pure else {
            '@ip': self.ip,
            '@port': self.port,
            '@workerid': self.workerid,
            '@hostarch': self.hostarch,
        })

        return xmltodict.unparse(
            {
                'worker': meta,
            }, pretty=True,
        )

    def asdict(self):
        return asdict(self)

    def update(self, worker):
        if worker.ip:
            self.ip = worker.ip
        self.port = worker.port
        self.hostarch = worker.hostarch
        if worker.meta:
            self.meta = worker.meta

    @classmethod
    def fromdict(cls, d):
        return cls(**d)

    @classmethod
    def fromxml(cls, xml, **extra):
        """
        Create a Worker object from an XML

        >>> Worker.fromxml('''
        ... <worker hostarch="armv8l" ip="1.2.3.4" port="1234" workerid="worker-1:1">
        ...   <linux>
        ...     <version>4.19.0-14</version>
        ...     <flavor>arm64</flavor>
        ...   </linux>
        ...   <hardware>
        ...     <cpu />
        ...     <processors>8</processors>
        ...   </hardware>
        ... </worker>''')
        Worker(port=1234, hostarch='armv8l', ip='1.2.3.4',
            meta={'linux':
                  {'version': '4.19.0-14',
                   'flavor': 'arm64'},
                'hardware':
                  {'cpu': None,
                   'processors': '8'}},
            workerid='worker-1:1')
        >>> Worker.fromxml('''
        ... <worker hostarch="aarch64" ip="4.3.2.1" port="4321" workerid="worker-2:2">
        ...   <sandbox>chroot</sandbox>
        ...   <linux>
        ...     <version>4.19.0-5</version>
        ...     <flavor>arm64</flavor>
        ...   </linux>
        ...   <hardware>
        ...     <cpu />
        ...     <processors>24</processors>
        ...   </hardware>
        ...   <job>debian:bookworm::default::systemd-4834c55cd01c5ef054f079c3af4f9533</job>
        ...   <arch>aarch64</arch>
        ...   <jobid>211438592abc7892a636a711f9dc339c</jobid>
        ... </worker>''')
        Worker(port=4321, hostarch='aarch64', ip='4.3.2.1',
            meta={'sandbox': 'chroot',
                  'linux':
                    {'version': '4.19.0-5',
                     'flavor': 'arm64'},
                  'hardware':
                    {'cpu': None,
                     'processors': '24'},
                  'job': 'debian:bookworm::default::systemd-4834c55cd01c5ef054f079c3af4f9533',
                  'arch': 'aarch64',
                  'jobid': '211438592abc7892a636a711f9dc339c'},
            workerid='worker-2:2')
        """
        data = xmltodict.parse(xml)['worker']
        new = {
            k.name: data[f'@{k.name}']
            for k in fields(cls)
            if f'@{k.name}' in data
        }
        new.update(meta={k: data[k] for k in data if not k.startswith('@')})
        new.update({k: extra[k] for k in extra if extra[k] is not None})
        return cls(**new)


@dataclass
class ProxiedWorker(Worker):
    externalport: int = None
    state: str = None

    def __post_init__(self):
        self.queue = asyncio.Queue()

    def __hash__(self):
        return hash(self.workerid)

    def internal(self):
        return self

    def external(self):
        return Worker(
            ip=self.ip,
            port=self.externalport,
            hostarch=self.hostarch,
            meta=self.meta,
            workerid=self.workerid,
        )
