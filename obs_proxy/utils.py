#!/usr/bin/python3
#
# OBS Proxy: various utilities
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
#
# Author: Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from __future__ import annotations

import datetime
import logging
import shelve
import sys
from logging import debug, error, info
from pathlib import Path
from typing import Mapping

import httpx
import structlog
from aiofiles.tempfile import NamedTemporaryFile
from aiofiles.threadpool import AsyncFileIO
from hypercorn.asyncio import serve
from hypercorn.config import Config as HyperConfig
from jsonrpc.jsonrpc2 import JSONRPC20Request, JSONRPC20Response
from quart import request, stream_with_context

from .chunked_uploads.client import (
    ChunkedUploadError,
    ChunkedUploadVerificationError,
    upload_chunked,
)
from .config import config, find_cache

MAXIMUM_PAYLOAD = 2 * 1024 * 1024 * 1024


# TODO: merge with print_ws_request
def format_ws_request(message):
    return message.get('params', {}) | {
        'id': message['id'] or '()',
        'method': message['method'],
    }


# TODO: merge with print_ws_response
def format_ws_response(message):
    return message | {
        'id': message['id'] or '()',
    }


def configure_logging():
    timestamper = structlog.processors.TimeStamper(fmt="%Y-%m-%dT%H:%M:%SZ", utc=True)
    shared_processors = [
        structlog.stdlib.add_log_level,
        structlog.contextvars.merge_contextvars,
        timestamper,
    ]

    structlog.configure(
        processors=shared_processors + [
            structlog.processors.CallsiteParameterAdder(
                [structlog.processors.CallsiteParameter.FUNC_NAME],
                additional_ignores=[
                ],
            ),
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )

    def add_extra(_, __, event_dict):
        # currently nop
        return event_dict

    formatter = structlog.stdlib.ProcessorFormatter(
        # These run ONLY on `logging` entries that do NOT originate within
        # structlog.
        foreign_pre_chain=shared_processors,
        # These run on ALL entries after the pre_chain is done.
        processors=[
            structlog.stdlib.add_logger_name,
            structlog.stdlib.PositionalArgumentsFormatter(),
            add_extra,
            structlog.processors.StackInfoRenderer(),
            # Remove _record & _from_structlog.
            structlog.stdlib.ProcessorFormatter.remove_processors_meta,
            structlog.dev.ConsoleRenderer() if sys.stdout.isatty() else structlog.processors.LogfmtRenderer(
                key_order=(
                    'timestamp',
                    'event',
                    'kind',
                    'level',
                ),
                drop_missing=True,
            ),
        ],
    )

    handler = logging.StreamHandler()
    # Use OUR `ProcessorFormatter` to format all `logging` entries.
    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)

    root_logger.setLevel(logging.DEBUG if config.debug else logging.INFO)

    # Too verbose for us
    quiet_loggers = [
        "websockets.client",
        "httpcore",
        "hpack.hpack",
        "hpack.table",
    ]
    for logger_name in quiet_loggers:
        logging.getLogger(logger_name).setLevel(logging.INFO)


def shorten(text: str, width: int = 512, placeholder: str = "...") -> str:
    r"""
    Collapse and truncate the given text to fit in the given width.

    Unlike textwrap.shorten, it removes text from the middle, not the end,
    and doesn’t collapse spaces.

    >>> text = ''.join(chr(x) for x in range(32, 127)) * 3
    >>> shorten(text, 100)
    ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOP...OPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
    >>> shorten(text, 11, ' <...> ')
    ' ! <...> }~'
    >>> shorten(text, 10, '...')
    ' !"#...|}~'
    >>> text = '1234567890123'
    >>> shorten(text, 10, ' <...> ')
    '12 <...> 3'
    >>> shorten(text, 9, ' <...> ')
    '1 <...> 3'
    >>> shorten(text, 11, ' <...> ')
    '12 <...> 23'
    """
    if len(text) > width:
        half_holder = len(placeholder) // 2
        half_width = width // 2
        return (
            text[:half_width - half_holder] +
            placeholder +
            text[len(text) - width + half_width + len(placeholder) - half_holder:]
        )
    else:
        return text


def print_ws_request(prefix, message):
    if 'worker_id' in message.get('params', {}):
        info(f"{prefix} #{message['id'] or '()'} [{message['method']}] {{{message['params']['worker_id']}}}")
    else:
        info(f"{prefix} #{message['id'] or '()'} [{message['method']}]")


def print_ws_response(prefix, message):
    if 'error' in message:
        error(f"{prefix} #{message['id'] or '()'} [{shorten('%r' % message['error'])}]")
    else:
        info(f"{prefix} #{message['id'] or '()'} [{shorten('%r' % message['result'])}]")


def print_ws_message(prefix, message):
    if isinstance(message, JSONRPC20Request):
        return print_ws_request(prefix, message.data)
    if isinstance(message, JSONRPC20Response):
        return print_ws_response(prefix, message.data)
    if isinstance(message, Mapping):
        if 'method' in message:
            return print_ws_request(prefix, message)
        if 'error' in message or 'result' in message:
            return print_ws_response(prefix, message)
    raise TypeError("message type unsupported", message)


def run_multiserver(
    app,
    host='127.0.0.1',
    https_ports=[6001],
    ports=[],
    debug=None,
    root_path: str = "",
    use_reloader=True,
    ca_certs=None,
    certfile=None,
    keyfile=None,
    shutdown_trigger=None,
):
    """Run a Quart app on multiple ports using Hypercorn"""
    config = HyperConfig()
    config.access_log_format = "%(h)s %(R)s %(s)s %(b)s %(D)s"
    config.accesslog = logging.getLogger("http.server")

    config.bind = [f'{host}:{port}' for port in https_ports] or [
        f'{host}:{port}' for port in ports
    ]
    config.insecure_bind = [f"{host}:{port}" for port in ports]
    config.ca_certs = ca_certs if https_ports else None
    config.certfile = certfile if https_ports else None
    if debug is not None:
        app.debug = debug
    config.errorlog = config.accesslog
    config.keyfile = keyfile if https_ports else None
    config.root_path = root_path
    config.use_reloader = use_reloader
    return serve(app, config, shutdown_trigger=shutdown_trigger)


def filterdict(d, keep=None, remove=None):  # noqa: E131
    """Filter a dict removing keys in `remove` and keeping keys in `keep`"""
    return {
        k: v for k, v in d.items()
        if (
            not remove or k not in remove
        ) and (
            not keep or k in keep
        )
    }


async def upload_data(
    client: httpx.AsyncClient,
    uri: str,
    buffer: bool,
    chunked_upload_uri: str | None = None,
    params: Mapping[str, str] | None = None,
    headers=None,
    **kv,
):
    """
    Upload data to a specified URI and return the response

    Upload is performed either as a single request or (if over a certain size) as a chunked upload.
    """
    if headers is None:
        headers = {}
    for header in ('content-type', 'content-length'):
        if header in request.headers:
            headers[header] = request.headers[header]
    if buffer:
        async with NamedTemporaryFile(delete=False) as tmpfile:
            debug(f"Buffering into {tmpfile.name}")
            async for data in request.body:
                await tmpfile.write(data)

            payload_size = await tmpfile.tell()

            # don’t overwrite the header just in case the upload was incomplete
            if 'content-length' not in headers:
                headers['content-length'] = str(payload_size)

            debug(f"Uploading to {uri} from {tmpfile.name}")
            try:
                if chunked_upload_uri and payload_size > MAXIMUM_PAYLOAD:
                    debug("Performing chunked upload")
                    await upload_chunked(
                        client,
                        chunked_upload_uri,
                        data=tmpfile,
                        size=payload_size,
                    )
                    r = await client.post(
                        uri,
                        headers=filterdict(headers, remove="content-length"),
                        params=params | {'chunked_upload': '1'},
                        **kv,
                    )
                else:
                    r = await upload_file(
                        client,
                        uri,
                        file=tmpfile,
                        params=params,
                        headers=headers,
                        **kv,
                    )
                Path(tmpfile.name).unlink()
                return r
            except httpx.RequestError as e:
                debug(f"{e = }")
                debug(f"{e.request = }")
                debug(f"Preserving {tmpfile.name} for further analysis.")
                raise
            except (ChunkedUploadError, ChunkedUploadVerificationError) as e:
                debug(f"{e = }")
                Path(tmpfile.name).unlink()
                debug(f"Deleted {tmpfile.name} as it is too large to preserve.")
                raise
            except httpx.HTTPError as e:
                debug(f"{e = }")
                debug(f"Preserving {tmpfile.name} for further analysis.")
                raise
    else:
        return await upload_request_stream(
            client,
            uri,
            params=params,
            headers=headers,
            **kv,
        )

UPLOAD_CHUNK_SIZE = 1024 * 1024


async def upload_file(
    client: httpx.AsyncClient,
    uri: str,
    file: AsyncFileIO,
    params: Mapping[str, str] | None = None,
    headers: Mapping[str, str] | None = None,
    **kv,
) -> httpx.Response:
    """Upload a file asynchronously and return a response."""

    async def upload_bytes():
        chunk = await file.read(UPLOAD_CHUNK_SIZE)
        while chunk:
            yield chunk
            chunk = await file.read(UPLOAD_CHUNK_SIZE)

    await file.seek(0)
    return await client.post(
        uri,
        content=upload_bytes(),
        params=params,
        headers=headers,
        **kv,
    )


async def upload_request_stream(
    client: httpx.AsyncClient,
    uri: str,
    params: Mapping[str, str] | None = None,
    headers: Mapping[str, str] | None = None,
    **kv,
) -> httpx.Response:

    @stream_with_context
    async def upload_bytes():
        async for data in request.body:
            yield data

    debug(f"Uploading stream to {uri}")
    try:
        return await client.post(
            uri,
            content=upload_bytes(),
            params=params,
            headers=headers,
            **kv,
        )
    except httpx.HTTPError as e:
        print(f"{e = }")
        raise


job_traces = {}


def job_trace(prefix: str, job_id: str, worker_id: str, text: str):
    if not config.tracedir:
        return

    global job_traces
    now = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    try:
        if job_id in job_traces:
            p = job_traces[job_id]
            f = p.resolve().open('at')
        else:
            p = (config.tracedir / f"job.{now}.{job_id}.log").resolve()
            f = p.open('at')
            job_traces[job_id] = p
            print(f"=== Traces for job {job_id}", file=f)
        print(f"[{now}] {prefix} {worker_id}:{text}", file=f)
        f.close()
    except OSError:
        logging.exception("Ignoring exception in job_trace:")
        pass


def job_alias(old_id: str, new_id: str):
    if not config.tracedir:
        return

    global job_traces
    try:
        if (old_id in job_traces) and (new_id not in job_traces):
            p = job_traces[old_id]
            now = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            new_p = config.tracedir / f"job.{now}.{new_id}.log"
            new_p.symlink_to(p.name)
            job_traces[new_id] = new_p
            with p.open('at') as f:
                print(f"[{now}] === aliased new id {new_id} to old id {old_id}", file=f)
    except OSError:
        logging.exception("Ignoring exception in job_alias:")
        pass


def open_cache(name: str):
    cache_file = find_cache(name)
    try:
        cache = shelve.open(str(cache_file))
    except Exception:  # noqa: B902
        # wrong format or empty, delete and try again
        cache_file.unlink(missing_ok=True)
        cache = shelve.open(str(cache_file))
    cache.path = cache_file
    return cache
