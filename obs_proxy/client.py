#!/usr/bin/python3
#
# OBS Proxy: client side
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020-2022 Collabora Ltd
# Copyright (c) 2020-2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import os
import signal
import sys

import structlog

from .config import config
from .repserver import repserver
from .utils import configure_logging, run_multiserver
from .wsclient import queue, wsclient

logger = structlog.get_logger()


def run():
    configure_logging()

    if config.server.insecure:
        logger.warning("Not verifying certificates when connecting to the OBS proxy server")
        logger.warning("This configuration should NOT be used in production!")

    loop = asyncio.get_event_loop()
    shutdown_event = asyncio.Event()

    def _terminate():
        logger.info("Terminating.")
        os._exit(0)

    def _signal_handler(*e):
        shutdown_event.set()
        logger.info("Received a signal, will terminate")
        loop.call_later(1, _terminate)

    try:
        loop.add_signal_handler(signal.SIGTERM, _signal_handler)
        loop.add_signal_handler(signal.SIGINT, _signal_handler)
    except (AttributeError, NotImplementedError):
        pass

    worker = run_multiserver(
        repserver,
        host='0.0.0.0',
        https_ports=[],
        ports=[config.client.port],
        shutdown_trigger=shutdown_event.wait,
        debug=False,
    )
    wsworker = wsclient(queue, shutdown_trigger=shutdown_event.wait)
    tasks = asyncio.gather(worker, wsworker)

    try:
        loop.run_until_complete(tasks)
    except Exception:  # noqa: B902
        logger.exception("Caught an exception:")
        sys.exit(1)
    finally:
        try:
            tasks = [task for task in asyncio.all_tasks(loop) if not task.done()]
            logger.debug(f"{tasks = }")
            if tasks:
                for task in tasks:
                    task.cancel()
                loop.run_until_complete(
                    asyncio.gather(*tasks, return_exceptions=True),
                )
                loop.run_until_complete(loop.shutdown_asyncgens())
        finally:
            asyncio.set_event_loop(None)
            loop.close()


if __name__ == '__main__':
    run()
