#!/usr/bin/python3
#
# OBS Proxy: client side
# RepoServer/SourceServer API proxy
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020-2022 Collabora Ltd
# Copyright (c) 2020-2022 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from typing import Optional

import httpx
import structlog
from jsonrpc.jsonrpc import JSONRPC20Request
from quart import Quart, make_response, request

from .config import HTTP_TIMEOUT, config
from .utils import filterdict, upload_data
from .worker import Worker
from .wsclient import first_connect, queue, workers  # noqa: F401

logger = structlog.get_logger()

repserver = Quart(__name__)
repserver.config['MAX_CONTENT_LENGTH'] = None

client = httpx.AsyncClient(
    http2=config.server.http2,
    verify=not config.server.insecure,
    timeout=HTTP_TIMEOUT,
    limits=httpx.Limits(max_connections=2048, max_keepalive_connections=256),
)


def proxy_server_uri(path: str, upstream: str = "worker", worker_id: Optional[str] = None) -> str:
    if not worker_id:
        worker_id = '-'
    scheme = 'https' if config.server.tls else 'http'
    return f"{scheme}://{config.server.host}:{config.server.port}{config.server.prefix}/{upstream}/{worker_id}/{path}"


def build_auth():
    if config.auth.username:
        return config.auth.username, config.auth.password
    else:
        def authenticate(req):
            token = config.auth.token
            req.headers["Authorization"] = f"Bearer {token}"
            return req

        return authenticate


@repserver.before_request
async def log_port():
    _, port = request.scope['server']
    structlog.contextvars.bind_contextvars(
        http_port=port,
    )


@repserver.route('/getbuildcode')
@repserver.route('/getworkercode')
@repserver.route('/<upstream>/<path:worker_id>/badpackagebinaryversionlist')
@repserver.route('/<upstream>/<path:worker_id>/build/<project>/<repository>/<arch>/<package>')
@repserver.route('/<upstream>/<path:worker_id>/getbinaries')
@repserver.route('/<upstream>/<path:worker_id>/getbinaryversions')
@repserver.route('/<upstream>/<path:worker_id>/getbuildcode')
@repserver.route('/<upstream>/<path:worker_id>/getconfig')
@repserver.route('/<upstream>/<path:worker_id>/getjobdata')
@repserver.route('/<upstream>/<path:worker_id>/getobsgendiffdata')
@repserver.route('/<upstream>/<path:worker_id>/getpackagebinaryversionlist')
@repserver.route('/<upstream>/<path:worker_id>/getpreinstallimageinfos')
@repserver.route('/<upstream>/<path:worker_id>/getsources')
@repserver.route('/<upstream>/<path:worker_id>/getsslcert')
@repserver.route('/<upstream>/<path:worker_id>/getworkercode')
@repserver.route('/<upstream>/<path:worker_id>/source/<project>')
@repserver.route('/<upstream>/<path:worker_id>/source/<project>/<package>')
async def proxy_request(upstream=None, worker_id=None, **kv):
    structlog.contextvars.bind_contextvars(
        upstream=upstream,
        worker_id=worker_id,
    )
    logger.info("proxy_request", kv=kv)
    path_parts = request.path.split('/')
    if upstream:
        uri = proxy_server_uri('/'.join(path_parts[3:]), upstream, worker_id)
    else:
        uri = proxy_server_uri(path_parts[1], 'repserver')

    logger.info(f"Proxying {uri}")

    async def async_generator(resp):
        try:
            async for chunk in resp.aiter_bytes():
                yield chunk
        finally:
            await resp.aclose()

    params = dict(request.args)
    if params.get('nometa') == '':
        params['nometa'] = 1

    req = client.build_request('GET', uri, params=params)
    try:
        resp = await client.send(req, stream=True, auth=build_auth())
    except httpx.HTTPError as e:
        logger.exception(f"=!= HTTP error for GET {uri}:")
        return str(e), 500
    response = await make_response(
        async_generator(resp), resp.status_code, filterdict(resp.headers, ['content-type', 'content-length', 'cache-control']),
    )
    response.timeout = HTTP_TIMEOUT
    return response


@repserver.route('/putjob', methods=['POST'])
@repserver.route('/repserver/<path:worker_id>/putjob', methods=['POST'])
async def putjob(worker_id=None, **kv):
    uri = proxy_server_uri('putjob', 'repserver', worker_id)
    job_id = request.args.get('jobid')
    structlog.contextvars.bind_contextvars(
        upstream='repserver',
        worker_id=worker_id,
        job_id=job_id,
    )
    if 'job' in kv:
        structlog.contextvars.bind_contextvars(job_name=kv['job'])
    chunked_upload_uri = proxy_server_uri(f'jobs/{job_id}', 'repserver', worker_id)
    logger.info(f"Proxying {uri}")

    try:
        resp = await upload_data(
            client, uri,
            buffer=config.client.buffer_uploads,
            chunked_upload_uri=chunked_upload_uri,
            params=request.args,
            auth=build_auth(),
        )
    except httpx.HTTPError as e:
        logger.exception(f"=!= HTTP error for POST {uri}:")
        return str(e), 500
    return resp.content, resp.status_code


@repserver.route('/worker', methods=['POST', 'GET'])
@repserver.route('/repserver/<path:worker_id>/worker', methods=['POST', 'GET'])
async def worker_ping(worker_id=None):
    global workers
    data = await request.get_data()
    state = request.args.get('state')
    try:
        peer_port = request.args.get('port')
        if peer_port:
            peer_port = int(peer_port)
    except ValueError:
        return "Invalid port number", 400
    host_arch = request.args.get('arch')
    worker_id = request.args.get('workerid', worker_id or (f"{request.remote_addr}:{peer_port}" if peer_port else None))
    structlog.contextvars.bind_contextvars(
        upstream='repserver',
        worker_id=worker_id,
    )
    if data:
        worker = Worker.fromxml(
            data,
            ip=request.remote_addr,
            port=peer_port,
            workerid=worker_id,
            hostarch=host_arch,
        )
        logger.info(f"    worker data: {worker}")
        workers[worker.workerid] = worker
        first_connect.set()
    else:
        worker = workers.get(worker_id, None)
        if not worker:
            logger.error(f" E  previously unknown worker updated: {worker_id}")
            return f"worker unknown: {worker_id}", 404
        worker.port = peer_port
        worker.hostarch = host_arch
    logger.info(f"    {worker.hostarch}:{worker.workerid} {state or 'unknown'} on {worker.port}")
    queue.put_nowait(
        JSONRPC20Request(
            method='worker_ping', params={'worker': worker.asdict(), 'state': state, 'worker_id': worker.workerid},
        ),
    )
    return 'ok'
