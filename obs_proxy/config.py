#!/usr/bin/python3
#
# OBS Proxy: configuration
#
# SPDX-License-Identifier: MPL-2.0
#
# Copyright (c) 2020 Collabora Ltd
# Copyright (c) 2020 Andrej Shadura <andrewsh@collabora.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import sys
from configparser import ConfigParser
from dataclasses import dataclass, is_dataclass
from logging import error
from pathlib import Path
from typing import Mapping

HTTP_TIMEOUT = 20 * 60


try:
    from xdg import XDG_CACHE_HOME, XDG_CONFIG_DIRS
except ImportError:
    try:
        from xdg import BaseDirectory
        XDG_CACHE_HOME = Path(BaseDirectory.xdg_cache_home)
        XDG_CONFIG_DIRS = [Path(path) for path in BaseDirectory.xdg_config_dirs]
    except ImportError:
        XDG_CACHE_HOME = Path.home() / '.cache'
        XDG_CACHE_HOME.mkdir(exist_ok=True)
        XDG_CONFIG_DIRS = [Path.home() / '.config']


@dataclass
class ServerConfig:
    '''"server" config section'''

    host: str
    port: int
    prefix: str
    http2: bool
    tls: bool
    insecure: bool
    buffer_uploads: bool
    keyfile: Path = None
    certfile: Path = None

    def __post_init__(self):
        self.keyfile = path_or_none(self.keyfile)
        self.certfile = path_or_none(self.certfile)


@dataclass
class ClientConfig:
    '''"client" config section'''

    host: str
    port: int
    buffer_uploads: bool


@dataclass
class AuthConfig:
    '''"auth" config section'''

    username: str
    password: str
    token: str


@dataclass
class BackendConfig:
    '''"backend" config section'''

    srcserver_uri: str
    repserver_uri: str


def path_or_none(p: Path):
    return Path(p) if p else None


@dataclass
class Config:
    '''Configuration parser'''

    server: ServerConfig
    client: ClientConfig
    auth: AuthConfig
    backend: BackendConfig
    worker_ports: range
    debug: bool
    tracedir: Path
    cachedir: Path

    @classmethod
    def parse_file(cls, f):
        parser = ConfigParser()
        print(f"Parsing {f}")
        if isinstance(f, os.PathLike) or isinstance(f, str):
            f = open(f)
        parser.read_file(f)
        if "workers" in parser.sections():
            startport = parser["workers"].getint("startport")
            endport = parser["workers"].getint("endport")
            worker_ports = range(startport, endport + 1)
        else:
            worker_ports = None

        server = {
            "host": parser["server"].get("host"),
            "port": parser["server"].getint("port", 6000),
            "prefix": parser["server"].get("prefix", "").rstrip("/"),
            "http2": parser["server"].getboolean("http2", False),
            "tls": parser["server"].getboolean("tls", False),
            "insecure": parser["server"].getboolean("insecure", False),
            "keyfile": parser["server"].get("keyfile"),
            "certfile": parser["server"].get("certfile"),
            "buffer_uploads": parser["server"].getboolean("buffer_uploads", False),
        }

        if "client" not in parser.sections():
            parser.add_section("client")

        client = {
            "host": parser["client"].get("host", "0.0.0.0"),
            "port": parser["client"].getint("port", 5000),
            # buffer uploads on clients only
            "buffer_uploads": parser["client"].getboolean("buffer_uploads", True),
        }
        if parser.get("client", "proxy", fallback=None):
            error("HTTP proxies are no longer supported, please adjust your configuration and remove proxy=.")
            sys.exit(1)

        if "debug" not in parser.sections():
            parser.add_section("debug")

        debug = (
            parser["server"].getboolean("debug", False) or
            parser["client"].getboolean("debug", False) or
            parser["debug"].getboolean("enabled", False)
        )
        tracedir = path_or_none(
            parser["server"].get("tracedir", None) or
            parser["client"].get("tracedir", None) or
            parser["debug"].get("tracedir", None),
        )

        auth = {
            "username": parser["auth"].get("username"),
            "password": parser["auth"].get("password"),
            "token": parser["auth"].get("token") or os.getenv("OBS_PROXY_AUTH_TOKEN"),
        }

        cachedir = path_or_none(parser["cache"].get("cachedir", None)) if 'cache' in parser else None

        if "backend" in parser.sections():
            srcserver_host, srcserver_port = parser["backend"]["srcserver"].split(":")
            repserver_host, repserver_port = parser["backend"]["repserver"].split(":")
            backend_host = parser["backend"].get("host")

            backend = {
                "srcserver_uri": "http://%s:%s" % (srcserver_host or backend_host, srcserver_port),
                "repserver_uri": "http://%s:%s" % (repserver_host or backend_host, repserver_port),
            }
        else:
            backend = {
                "srcserver_uri": None,
                "repserver_uri": None,
            }

        return cls(
            server=ServerConfig(**server),
            client=ClientConfig(**client),
            auth=AuthConfig(**auth),
            backend=BackendConfig(**backend),
            worker_ports=worker_ports,
            debug=debug,
            tracedir=tracedir,
            cachedir=cachedir,
        )

    def __post_init__(self):
        if self.tracedir:
            self.tracedir.mkdir(parents=True, exist_ok=True)

    def as_dict(self):
        def serialize(o):
            if is_dataclass(o):
                return serialize(o.__dict__)
            if isinstance(o, Mapping):
                return {k: serialize(v) for k, v in o.items()}
            if isinstance(o, range):
                return {'start': o.start, 'end': o.stop - 1}
            if isinstance(o, os.PathLike):
                return str(o)
            return o

        return serialize(self)

    def update_from_dict(self, d):
        def deserialize(o, c, d):
            if is_dataclass(o):
                for k in d.keys():
                    if k in o.__annotations__:
                        cls = o.__annotations__[k]
                        o.__dict__[k] = deserialize(o.__dict__[k], cls, d[k])
            elif c is range:
                o = range(d['start'], d['end'] + 1)
            elif c is Path:
                o = path_or_none(d)
            else:
                o = d
            return o

        deserialize(self, type(self), d)


appname = 'obs-proxy'


def find_config() -> Path:
    places = (Path(d) / appname for d in (*XDG_CONFIG_DIRS, '/etc', '/usr/lib'))
    for d in (*places, '.'):
        p = Path(d) / 'proxy.conf'
        if p.is_file():
            return p
    raise FileNotFoundError("Can't find proxy.conf")


def find_cache(name: str) -> Path:
    for d in (config.cachedir, Path('/var/cache') / appname, XDG_CACHE_HOME / appname):
        if not d:
            continue
        new_cache = d / name
        try:
            d.mkdir(parents=True, exist_ok=True)
            new_cache.touch()
            return new_cache
        except OSError:
            continue

    raise FileNotFoundError("Can't find suitable place for the cache")


config = Config.parse_file(find_config())
