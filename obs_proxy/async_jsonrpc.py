# Async support for JSON-RPC
#
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2013-2018 Kirill Pavlov <k@p99.io>
# Copyright (c)      2020 Collabora Ltd
# Copyright (c)      2020 Andrej Shadura <andrewsh@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json
import logging

import httpx
from jsonrpc.dispatcher import Dispatcher
from jsonrpc.exceptions import (
    JSONRPCDispatchException,
    JSONRPCInvalidParams,
    JSONRPCInvalidRequest,
    JSONRPCInvalidRequestException,
    JSONRPCMethodNotFound,
    JSONRPCParseError,
    JSONRPCServerError,
)
from jsonrpc.jsonrpc import JSONRPCRequest
from jsonrpc.jsonrpc2 import JSONRPC20Response
from jsonrpc.manager import JSONRPCResponseManager
from jsonrpc.utils import is_invalid_params

logger = logging.getLogger(__name__)

dispatcher = Dispatcher()


class AsyncJSONRPCResponseManager(JSONRPCResponseManager):
    @classmethod
    async def handle(cls, request, dispatcher):
        if isinstance(request, bytes):
            request = request.decode("utf-8")

        if isinstance(request, str):
            try:
                data = json.loads(request)
            except (TypeError, ValueError):
                return JSONRPC20Response(error=JSONRPCParseError()._data)
        else:
            data = request

        try:
            req = JSONRPCRequest.from_data(data)
        except JSONRPCInvalidRequestException:
            return JSONRPC20Response(error=JSONRPCInvalidRequest()._data)

        return await cls.handle_request(req, dispatcher)

    @classmethod
    async def handle_request(cls, request, dispatcher):
        """Handle request data.

        At this moment request has correct jsonrpc format.

        :param dict request: data parsed from request_str.
        :param jsonrpc.dispatcher.Dispatcher dispatcher:

        """
        response = await cls._get_response(request, dispatcher)

        # notifications
        if response:
            return response

    @classmethod
    async def _get_response(cls, request, dispatcher):
        """Response to each single JSON-RPC Request.

        :return iterator(JSONRPC20Response):

          TypeError inside the function is distinguished from Invalid Params.

        """
        def make_response(**kwargs):
            response = cls.RESPONSE_CLASS_MAP[request.JSONRPC_VERSION](
                _id=request._id, **kwargs,
            )
            response.request = request
            return response

        output = None
        try:
            method = dispatcher[request.method]
        except KeyError:
            output = make_response(error=JSONRPCMethodNotFound()._data)
        else:
            try:
                result = await method(*request.args, **request.kwargs)
            except JSONRPCDispatchException as e:
                output = make_response(error=e.error._data)
            except Exception as e:  # noqa: B902
                data = {
                    "type": e.__class__.__name__,
                    "args": e.args,
                    "message": str(e),
                }

                if isinstance(e, httpx.HTTPError):
                    data["url"] = str(e.request.url)

                # logger.exception("API Exception: {0}".format(data))
                # For traceback, use:
                # from traceback import format_exc
                #   "message": "\n".join((str(e), format_exc()))

                if isinstance(e, TypeError) and is_invalid_params(
                        method, *request.args, **request.kwargs,
                ):
                    output = make_response(
                        error=JSONRPCInvalidParams(data=data)._data,
                    )
                else:
                    output = make_response(
                        error=JSONRPCServerError(data=data)._data,
                    )
            else:
                output = make_response(result=result)
        if not request.is_notification:
            return output
