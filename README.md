OBS Proxy
=========

The OBS proxy can be used to link the [Open Build Service][] backend to
workers behind a NAT or a firewall.

The proxy is split in two parts, a client and a server. The server needs to
run in the same network as the backend, while the client usually runs in the
same network as the worker. Each client can proxy connections for multiple
workers.

Currently not supported:
 * Websockets connection over HTTP/2.
 * Better logging. Currently, a lot of tracing is being printed with no way
   to disable it.

License
=======

The project is licensed under the Mozilla Public License version 2.0.
See `COPYING.MPL` for more details.

Some individual files are licensed under different licenses, see copyright
notices in those files for more details:

 * `async_jsonrpc.py`
 * `rpcqueue.py`

[Open Build Service]: https://openbuildservice.org/
